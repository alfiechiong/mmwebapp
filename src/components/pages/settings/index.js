import React, {
    Component
  } from 'react';
 
  import {Consumer} from '../../AppProvider';
  import Contact from '../../childComponents/ContactInfo'
  import {BreadCrumb,CardLeft,CardRight,UsersContainer, Main} from './Styles';
  import TalkPerson from '../../childComponents/TalkPerson/module/AddTalkPerson';
  import ListTalkPerson from '../../childComponents/TalkPerson/module/ListTalkPersons';

  class Settings extends Component {

    render() {
      return (
        <Main>
        <Consumer>
          
          {({ state, ...context }) => (
            
        <div className='container'>
       
        {console.log(state.currentUser)}
        {state.uploadedSessionUrl && state.uploadedSessionUrl}
       <BreadCrumb>Home > Settings</BreadCrumb>
        
          <Contact />

          <hr />

          <br />
          <ListTalkPerson />
          <br />
        
        </div>
        )}
        </Consumer>
        </Main>
      );
    }
  }
  export default Settings;
