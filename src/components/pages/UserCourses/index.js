import React,{Component} from 'react';
import ListFreeCourses from '../../childComponents/Courses/module/listFreeCourses';
import ListPaidCourses from '../../childComponents/Courses/module/listPaidCourses';
import {Consumer} from '../../AppProvider'


class UserCourses extends Component {

    render(){
        return(
            
    <Consumer>
    {
      ({ state }) => 
      state.type === 'subscriber' ? 
            <ListPaidCourses />
            :
            <ListFreeCourses />
    }
    </Consumer>
        )
    }
}

export default UserCourses