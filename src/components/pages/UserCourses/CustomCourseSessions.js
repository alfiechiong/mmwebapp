import React,{Component} from 'react'
import {Link, withRouter} from 'react-router-dom'
import {Audioplayer} from '../../common/audioplayer'
import {Sessionlist} from './Styles'

class CustomCourseSessions extends Component {

    constructor(props){

        super(props)
    }

    render(){
        return(
            <div>
                    <Sessionlist>
                    <ul>
                    {  this.props.location.data && (this.props.location.data.map((itemData, i) =>(
                    <li key={i}>
{/*                         <div> {JSON.stringify(itemData.ses)}</div>

 */}                     
                         <div> {itemData.ses.data.name}</div>
                        <br />
                        <div> <Audioplayer track={itemData.ses.data.link} /></div>
                        <h3>Description</h3>
                        <div>{itemData.ses.data.description}</div> 
                    </li>
                    
                ))) }
                </ul>
    </Sessionlist> 
                course session
            </div>
        )
    }
}

export default withRouter(CustomCourseSessions) 