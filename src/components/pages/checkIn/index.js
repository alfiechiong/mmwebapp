import React, {
    Component
  } from 'react';
  import {withRouter} from 'react-router-dom';
  import {
    NumberCounterCountainer,
    CounterTextCountainer,
    HAYF,
    Main,
    Counter,
    Plus,
    Minus,
    ContinueButton} from './Styles';
  import ListAllUsers from '../../childComponents/User/ListUsers';
  import ActiveUserIconBig from '../../../assets/active_users.svg';
  import UserIcon from '../../../assets/inactive_users.svg';
  import {connect} from 'react-redux'
  import {BigLogo} from '../../common/Logo'
  import smiley0 from '../../../assets/SmileyIcon1.png'
  import smiley1 from '../../../assets/SmileyIcon2.png'
  import smiley2 from '../../../assets/SmileyIcon3.png'
  import smiley3 from '../../../assets/SmileyIcon4.png'
  import smiley4 from '../../../assets/SmileyIcon5.png'
  import smiley5 from '../../../assets/SmileyIcon6.png'
  import smiley6 from '../../../assets/SmileyIcon7.png'
  const mapStateToProps = state => {
    return { 
      dbUsers: state.user.dbUsers[0],
      userCount:state.user.userCount
    };
  };

  class Dashboard extends Component {

    constructor(props){
      super(props)

      this.state= {
        smile:[
          smiley0,
          smiley1,
          smiley2,
          smiley3,
          smiley4,
          smiley5,
          smiley6],
          counter:4
      }
        
    }

    onPlus = ()=>{
      if(this.state.counter < 7){
      this.setState({counter:this.state.counter + 1})
      }
    }

    onMinus = ()=>{
      if(this.state.counter > 1){
      this.setState({counter:this.state.counter - 1})
      }
    }

    goTo = ()=>{
      let link = ''

      if(this.state.counter <= 2) {
        link = 'helpline'
      }
       
      else if(this.state.counter <= 4) {
        link = 'trysession'
      }

      else if(this.state.counter > 4) {
        link = 'oneminute'
      } 

      this.props.history.push(link)
    }

    render() {
      
      return (
        
        <Main>  
          <img src={this.state.smile[this.state.counter-1]} />
          <HAYF>How Are You Feeling?</HAYF>
          <NumberCounterCountainer>
            <div>
            <div><Minus  onClick={this.onMinus}>-</Minus></div>
            <div><Counter>{this.state.counter}</Counter></div>
            <div><Plus onClick={this.onPlus}>+</Plus></div>
            </div>
          </NumberCounterCountainer>
          <CounterTextCountainer>
            Score your current mental health<br />
            state on a scale of 1 to 7

            <br /><br />
            1 being the worst <br />
            7 being the best
          </CounterTextCountainer>

          <ContinueButton onClick={()=>this.goTo()}>Continue</ContinueButton>
        </Main>
       
      );
    }
  }
  export default connect(mapStateToProps)(withRouter(Dashboard));
  //export default Dashboard;