import styled, { css } from 'styled-components'
import {media} from '../../common/media';

export const Main = styled.div`

@media(max-width: 400px) {
  min-width:400px;
  max-width:400px;
}

@media(max-width: 360px) {
  min-width:300px;
  max-width:300px;
}

@media(max-width: 411px) {
  min-width:400px;
  max-width:400px;
}
    text-align: center;
    width:800px;
    margin:0px auto;
    overflow:auto;
    clear:both;

    img{
      width:100px;
      margin:30px 10px 10px 10px;
    }
`

export const HAYF = styled.div`

padding:20px;
box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
margin:10px 0
text-align:center;
color:white;
background-color:rgb(85, 60, 131);
font-size:20px;
`

export const NumberCounterCountainer = styled.div`
margin:20px 0 0 0;
padding:20px;
background:#ccc;
border-radius:5px 5px 0 0;
display:flex;

&>div{
  display:flex;
  flex:1;
  align-items: center;
  justify-content: center;

  div{
  display:flex;
  align-items: center;
  justify-content: center;
  }
}

`

export const Indec = styled.div`
  height:100%;
  display: flex;
  align-items: center;
` 

export const Counter = styled.div`

  width:100px;
  height:100px;
  background:white;
  border-radius:25px;
  padding:20px;
  margin:20px;
  font-size:70px;
  display:flex;
  font-weight:600;
  vertical-align: middle;
  line-height: 100px;       
`

export const Plus = styled.div`
width:40px;
height:40px;
border-radius:0 15px 15px 0;
background-color:#676767;
padding:10px;
vertical-align: middle;
font-size:30px;
color:#fff;
font-weight:bold;
cursor:pointer;
`
export const Minus = styled.div`
cursor:pointer;
width:40px;
height:40px;
border-radius: 15px 0 0 15px ;
background-color:#676767;
padding:10px;
vertical-align: middle;
font-size:30px;
color:#fff;
font-weight:bold;
`

export const CounterTextCountainer = styled.div`
margin:0;
padding:20px;
background:white;
border-radius:0 0 5px 5px ;
`

export const ContinueButton= styled.div`
margin:20px 0;
padding:20px;
background:white;
border-radius:5px ;
cursor:pointer;
font-size:25px;
:hover{
  background-color:#ccc;
}
`


export const BreadCrumb = styled.div`
  padding:10px;
  text-align:left;
  font-size:16;
  font-color:#ccc;
  font-weight:bold;
  background-color:#ddf4f7;
  margin:20px auto;
  border-radius:10px;

`


export const CardLeft = styled.div`
    font-family: Montserrat;
    width:340px;
    background:#df8500;
    border-radius:15px;
    padding:10px
    height:160px;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    float:left;
    margin:20px;


    .head {
      font-size:18px;
      font-weight:bold;
      color:white;
    }

    .left {
      margin-top:10px;
      margin-left:10px;
      float:left;
    }

    .right {
      float:left;
    
    }

    & > div {
      & > div{
        padding-top:20px;
        padding-right:10px;
        font-size:75px;  
        font-weight:600;
        color:white;  
        
      }

      & > span {
        font-size: 13px;
        font-weight: 600;
        font-style: normal;
        font-stretch: normal;
        line-height: normal;
        letter-spacing: normal;
        text-align: left;
        color: #333333;
      }
      
    } 
    }
    `

export const Desc = styled.span`
font-family: Montserrat;
    max-width:450px;
    min-width:300px;
    background:#cccccc2b;
    border-radius:0 0 15px 15px;
    padding:10px
    
    display:flex;
    margin:0px auto 20px auto;

    cursor:pointer;
`

export const Choices = styled.div`
    font-family: Montserrat;
    max-width:400px;
    min-width:340px;
    background:rgb(222, 222, 224)
    ;
    border-radius:5px;
    padding:10px
    height:50px;
    display:flex;
    margin:8px auto 0 auto;
    cursor:pointer;
 

    @media (max-width: 400px) {
      min-width:280px;
      max-width:280px;
    }

    @media (max-width: 411px) {
      min-width:280px;
      max-width:280px;
    }

    @media (max-width: 360px) {
      min-width:250px;
      max-width:250px;
      margin:0;
      padding:0;
    }

    

    &:hover {
      background:rgb(209 225 251);
    }

    .head {
      font-size:18px;
      font-weight:bold;
      color:white;
    }

    .left {
      margin-top:10px;
      margin-left:10px;
      float:left;
    }

    .right {
      float:left;
    
    }

    & > div {
   
      @media (max-width: 400px) {
        font-size:16px;  
      }

      font-size:30px;  
      font-weight:600;
      color:#666;  
     
      margin: auto;

      
      
      & > div{
        padding-top:20px;
        padding-right:10px;
        font-size:30px;  
        font-weight:600;
        color:white;  
        text-align:center;
        padding:0 auto;
        
      }

      & > span {
        font-size: 13px;
        font-weight: 600;
        font-style: normal;
        font-stretch: normal;
        line-height: normal;
        letter-spacing: normal;
        text-align: left;
        color: #333333;
      }
      
    } 
    }
    `

    export const CardRight = styled.div`
    font-family: Montserrat;
    width:340px;
    border-radius:15px;
    background:#798491;
    padding:10px
    height:160px;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    float:right;
    margin:20px;

    .left {
      margin-top:20px;
      float:left;
      margin-top:10px;
      margin-left:10px;
    }

    .right {
      float:left;
      
    }
    .head {
      font-size:18px;
      font-weight:bold;
      color:white;
    }

    & > div {
      & > div{
        padding-top:20px;
        padding-right:10px;
        font-size:75px;  
        font-weight:600;
        color:white;
      }
      & > span {
        font-size: 13px;
        font-weight: 600;
        font-style: normal;
        font-stretch: normal;
        line-height: normal;
        letter-spacing: normal;
        text-align: left;
        color: #333333;
      }
    } 
    }
    `


 export const UsersContainer = styled.section`
 
 padding:20px;

 clear:both;
 margin-top:10px;
 `   

const btn = (light, dark) => css`
  white-space: nowrap;
  display: inline-block;
  border-radius: 5px;
  padding: 5px 15px;
  font-size: 16px;
  color: white;
  &:visited {
    color: white;
  }
  background-image: linear-gradient(${light}, ${dark});
  border: 1px solid ${dark};
  &:hover {
    background-image: linear-gradient(${light}, ${dark});
    &[disabled] {
      background-image: linear-gradient(${light}, ${dark});
    }
  }
  &:visited {
    color: black;
  }
  &[disabled] {
    opacity: 0.6;
    cursor: not-allowed;
  }
`

const btnDefault = css`
  ${btn('#ffffff', '#d5d5d5')} color: #555;
`

const btnPrimary = btn('#4f93ce', '#285f8f')
const btnDanger = btn('#e27c79', '#c9302c')

export default styled.div`
  font-family: sans-serif;
  overflow:auto;
  clear:both;
  h1 {
    text-align: center;
    color: #222;
  }

  h2 {
    text-align: center;
    color: #222;
  }

  & > div {
    text-align: center;
    width:800px;
    margin:0px auto;

    
  }

  a {
    display: block;
    text-align: center;
    color: #222;
    margin-bottom: 10px;
  }

  p {
    max-width: 500px;
    margin: 10px auto;
    & > a {
      display: inline;
    }
  }

  .left{
    float:left;
  }
  
  .right{
    float:right;
  }

  form {
    max-width: 500px;
    margin: 10px auto;
    border: 1px solid #ccc;
    padding: 20px;
    box-shadow: 2px 2px 5px rgba(0, 0, 0, 0.3);
    border-radius: 3px;
    position: relative;

    .loading {
      text-align: center;
      display: block;
      position: absolute;
      background: url('https://media.giphy.com/media/130AxGoOaR6t0I/giphy.gif')
        center center;
      background-size: fill;
      font-size: 2em;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      padding: 50px 0 0 0;
      z-index: 2;
    }

    & > div {
      display: flex;
      flex-flow: row nowrap;
      line-height: 2em;
      margin: 5px;
      position: relative;
      & > label {
        color: #333;
        width: 110px;
        min-width: 60px;
        font-size: 1em;
        line-height: 32px;
      }
      
      & > input,
      & > select,
      & > textarea {
        flex: 1;
        padding: 3px 5px;
        font-size: 1em;
        margin-left: 15px;
        border: 1px solid #ccc;
        border-radius: 3px;
      }
      & > input[type='checkbox'] {
        margin-top: 7px;
      }
      & > div {
        margin-left: 16px;
        & > label {
          display: block;
          & > input {
            margin-right: 3px;
          }
        }
      }
      & > span {
        line-height: 32px;
        margin-left: 10px;
        color: #800;
        font-weight: bold;
      }
      & > button.remove {
        ${btnDanger};
      }
    }
    & > .buttons {
      display: flex;
      flex-flow: row nowrap;
      justify-content: center;
      margin-top: 15px;
    }

    .error {
      display: flex;
      font-weight: bold;
      color: #800;
      flex-flow: row nowrap;
      justify-content: center;
    }
    pre {
      position: relative;
      border: 1px solid #ccc;
      background: rgba(0, 0, 0, 0.1);
      box-shadow: inset 1px 1px 3px rgba(0, 0, 0, 0.2);
      padding: 20px;
    }
  }
  button {
    margin: 0 10px;
    &[type='submit'] {
      ${btnPrimary};
    }
    &[type='button'] {
      ${btnDefault};
    }
  }
`
