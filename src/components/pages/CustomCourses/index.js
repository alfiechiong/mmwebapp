import React,{Component} from 'react';
import ListFreeCourses from '../../childComponents/Courses/module/listFreeCourses';
import ListCustomCourses from '../../childComponents/Courses/module/listCustomCourses';
import {Consumer} from '../../AppProvider'


class UserCourses extends Component {

    render(){
        return(
            
    <Consumer>
    {
      ({ state }) => 
      state.type === 'subscriber' ? 
            <ListCustomCourses />
            :
            ''
    }
    </Consumer>
        )
    }
}

export default UserCourses