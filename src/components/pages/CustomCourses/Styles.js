import styled from 'styled-components'

export const Sessionlist = styled.div`
border-collapse: collapse;
height:100%;
width:100%;
clear:both;
display:flex;
justify-content:center;

& > ul{
&>li{

box-shadow: 4.9px 4.9px 0 0 rgba(207, 207, 209, 0.75);
  color:white;
  background:lightslategrey;
  display:block;
  min-width:300px;
  max-width:500px;
  padding:20px;
  margin:20px auto;
  border-radius:10px;
}
}
}
` 