import React, {
    Component
  } from 'react';
  import {withRouter} from 'react-router-dom';
  import {BreadCrumb,Choices,CardRight,UsersContainer, Main,Desc} from './Styles';
  import ListAllUsers from '../../childComponents/User/ListUsers';
  import ActiveUserIconBig from '../../../assets/active_users.svg';
  import UserIcon from '../../../assets/inactive_users.svg';
  import {connect} from 'react-redux'
  import {BigLogo} from '../../common/Logo'
  
  const mapStateToProps = state => {
    return { 
      dbUsers: state.user.dbUsers[0],
      userCount:state.user.userCount
    };
  };

  class Dashboard extends Component {

    constructor(props){
      super(props)
        
    }

    render() {
      
      return (
        
        <Main>  
          <BigLogo />
          <Choices onClick={()=>{this.props.history.push('/checkin')}}  >
          <div>Check In</div>
    
        </Choices>   
   {/*        <BreadCrumb >Home</BreadCrumb> */}
        <Choices onClick={()=>{this.props.history.push('/oneminute')}}  >
          <div>One Minute Mindset</div>
    
        </Choices>  
{/*         <Desc>try one of our super short 'resets' - one minute to regain your balance, reset your mindset and give you an immediate mindset boost</Desc>
 */}
        <Choices onClick={()=>{this.props.history.push('/trysession')}} > 
          <div >Try a Session</div>
         
       </Choices>  
{/*        <Desc>try a short session and expereince the mindset maestro approach.  You can choose 'relax', 'calm' or 'mindset reset' sessions now.</Desc>
 */}
       <Choices onClick={()=>{this.props.history.push('/usercourses')}} > 
          <div >Courses</div>        
       </Choices>  
{/*        <Desc>Our courses, available after you join, multiple session courses designed to help you with common mindset issues to take you to your next level are always accessible to you.</Desc>
 */}
        <Choices onClick={()=>{this.props.history.push('/powerups')}}>

         <div>Try a Personal Power Up</div>

        </Choices>  

        
{/*         <Desc>The most powerful mindset tool available.  Answer a series of short questions and the algorithm will completely customise a minsert course just for you.  Personal power-ups are designed to get right to the heart of where you are at, and give you the personal power that you seek.</Desc>
 */}
       
        </Main>
       
      );
    }
  }
  export default connect(mapStateToProps)(withRouter(Dashboard));
  //export default Dashboard;