import React, {
    Component
  } from 'react';

  import {BreadCrumb,CardLeft,CardRight,UsersContainer, Main} from './Styles';
  import ListAllUsers from '../../childComponents/User/ListUsers';
  import ActiveUserIconBig from '../../../assets/active_users.svg';
  import UserIcon from '../../../assets/inactive_users.svg'; 
  import {connect} from 'react-redux'
  
  const mapStateToProps = state => {
    return { 
      dbUsers: state.user.dbUsers[0],
      userCount:state.user.userCount
    };
  };

  class Dashboard extends Component {

    constructor(props){
      super(props)
        
    }

    render() {
      
      return (
        
        <Main>  
          <BreadCrumb >Home</BreadCrumb>
        <CardLeft >
          <div className='head'>USER COUNT</div>
          <div className='left'><img src={UserIcon} width='100' height='100'/></div>
          <div className='right'><div>{this.props.userCount}</div>
          <span>Date: nov 13, 2019 </span>
          </div>

        </CardLeft>  
        <CardRight className='right'>
        <div className='head'>ACTIVE SUBCSRIBERS</div>
          <div className='left'><img src={ActiveUserIconBig} width='100' height='100'/></div>
          <div className='right'><div>{this.props.userCount}</div>
          <span>Date: nov 13, 2019 </span>
          </div>
        </CardRight>  
        
        
       
        <UsersContainer >
          <ListAllUsers />
          {console.log(this.props.userCount)}
        </UsersContainer>
        
        
        {/* <Consumer>
          
          {({ state, ...context }) => (
            
        <div className='container'>
       
        {console.log(state.currentUser)}
        {state.uploadedSessionUrl && state.uploadedSessionUrl}
        <h1>{state.currentUser.providerData[0].email} !!!</h1>
        
          <User />

          <hr />
          <Project />

          <br /><br />
           <h3>AUDIO Uploader</h3>
           <Uploader />
           <br/> <br />

           <Member />

           <br /><br /> 
           <h3>Wizard Form</h3>
           
        </div>
        )}
        </Consumer> */}
        </Main>
       
      );
    }
  }
  export default connect(mapStateToProps)(Dashboard);
  //export default Dashboard;