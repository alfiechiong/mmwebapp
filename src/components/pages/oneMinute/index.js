import React,{Component} from 'react'
import {Audioplayer} from '../../common/audioplayer'
import OneMinute from '../../childComponents/Session/OneMinute'
import {ContentPowerUp} from './Styles'

import AudioView from '../../common/audio/AudioView'
class OneMinuteSession extends Component {

    render(){
        return(
            <ContentPowerUp>
{/* try one of our super short 'resets' - one minute to regain your balance, reset your mindset and give you an immediate mindset boost
 */}            <OneMinute />

 <br />
            </ContentPowerUp>
        )

    }
}

export default OneMinuteSession;