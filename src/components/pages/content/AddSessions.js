import React, {
    Component
  } from 'react';

  import {ListSessions,AddSession} from '../../childComponents/Session';
  import Project from '../../childComponents/Project';
 
  import {Consumer} from '../../AppProvider';
  import Courses from '../../childComponents/Courses';
  import Uploader from '../../common/uploader';
  import {BreadCrumb, Main} from './Styles';
  import DropZone from '../../common/DropZone';
  
  class AddSessions extends Component {

    render() {
      return (
        <Main>
        <div className='container'>
          <BreadCrumb>Home > Content > Addsessions</BreadCrumb>  
          <DropZone />
        </div>
         
        </Main>
      );
    }
  }
  export default AddSessions;
