import React, {
    Component
  } from 'react';

  import {ListSessions,AddSession} from '../../childComponents/Session';
  import Project from '../../childComponents/Project';
 
  import {Consumer} from '../../AppProvider';
  import {AddCourses as NewCourse} from '../../childComponents/Courses/module';
  import Uploader from '../../common/uploader';
  import {BreadCrumb, Main} from './Styles';
  
  class AddCourses extends Component {

    render() {
      return (
        <Main>
        <Consumer>
          
          {({ state, ...context }) => (
            
        <div className='container'>
          <BreadCrumb>Home > Content > Addcourses</BreadCrumb>  
                <NewCourse />
            <hr />
        </div>
        )}
        </Consumer>
        </Main>
      );
    }
  }
  export default AddCourses;
