import React, {
    Component
  } from 'react';
  import {
    withRouter
  } from 'react-router-dom';
  import {ListSessions,AddSession} from '../../childComponents/Session';
  import Project from '../../childComponents/Project';
 
  import {Consumer} from '../../AppProvider';
  import {ListCourses} from '../../childComponents/Courses/module';
  import Uploader from '../../common/uploader';
  import {BreadCrumb, Main} from './Styles';
 
  
  class Content extends Component {

    render() {
      return (
        <Main>
        <Consumer>
          
          {({ state, ...context }) => (
            
        <div className='container'>
       
        {console.log(state.currentUser)}
 {/*        {state.uploadedSessionUrl && state.uploadedSessionUrl} */}
          <BreadCrumb>Home > Content</BreadCrumb>  
        
          {/* <Uploader /> */}
           <hr />
           <ListSessions /> 
           
           <hr />
            <ListCourses /> 
            <hr />
        </div>
        )}
        </Consumer>
        </Main>
      );
    }
  }
  export default withRouter(Content);
  //export default Dashboard;