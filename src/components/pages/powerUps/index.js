import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import Questionaire from '../../childComponents/Questionaire'
import {Consumer} from '../../AppProvider' 
import BeAMember from '../beamember'


class PowerUps extends Component {
render(){
    return(
        <Consumer >
             {({ state, ...context }) => (
      state.type === 'subscriber' ?
        <div>
        <Questionaire />
        
        <br /><br /><br /><br />
        </div>
        : <BeAMember />
             )}
        </Consumer>
    )
}

}

export default PowerUps