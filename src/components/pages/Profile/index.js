import React, {
    Component
  } from 'react';
  import {
    withRouter
  } from 'react-router-dom';
  import {Consumer} from '../../AppProvider';
  import { Main,ProfileList,Details,Box} from './Styles';
  import {HeadProfile,Arrow} from './component'

 
  class Profile extends Component {
    state={
      mindset:false,
      tracker:false,
      powerups:false
    }

    openDetails = (name)=>{
      this.setState({[name]:!this.state[name]})
    }

    render() {
      return (
        <Main>
        <HeadProfile name="alfiechigfn"></HeadProfile>
        
         <ProfileList>
           <li>
             <div onClick={()=>this.openDetails('mindset')}> <Arrow name='MINDSET RESET' open={this.state.mindset} />
              </div>
            <Details open={this.state.mindset}>
              <Box ></Box>
              <Box />
              <Box />
              </Details> 
              </li>
            <li >
            <div onClick={()=>this.openDetails('powerups')}>
              <Arrow name='POWER UPS' open={this.state.powerups}/>
            </div>
              <Details open={this.state.powerups}>
              <Box />
              <Box />
              <Box />
             
              </Details> 

              <Details open={this.state.powerups}>
              <Box />
              <Box />
              <Box />
              </Details> 
            </li>
            <li>
              <div onClick={()=>this.openDetails('tracker')}>
                <Arrow name='TRACKER' open={this.state.tracker} />
              </div>
              <Details open={this.state.tracker}>
              <Box />
              <Box />
              <Box />
              </Details> 
            </li>
        </ProfileList> 
        </Main>
      );
    }
  }
  export default withRouter(Profile);
