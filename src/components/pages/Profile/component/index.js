import React from 'react'
import {HeadStyle,Ar} from '../Styles'

export const HeadProfile = (props)=>(
<HeadStyle>
<span>{props.name}</span>
</HeadStyle>
)

export const Arrow = (props)=>(
   <div> {props.name}<Ar open={props.open}/></div>
)

