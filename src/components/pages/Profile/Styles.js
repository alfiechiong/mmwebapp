import styled, { css } from 'styled-components'
import headBg from '../../../assets/ProfileHeadBg.jpg'
import Arrow from '../../../assets/SmallArrow.svg'

export const Box = styled.div`


border:solid thin #cccc;

height:100px;
flex:1;
display:flex;
margin:10px;
:nth-child(3n) {
  break-after: always;
  background: silver;
  content: <br />;
}
`

export const Details = styled.div`
display:flex;
flex:1;
align-items: flex-start;
justify-content: space-between; 
max-height: ${props => props.open ? "100%" : "0"};
overflow: hidden;
padding: ${props => props.open ? "25px 0" : "0"};
opacity: ${props => props.open ? "1" : "0"};
flex-flow: row wrap;

transition: all 0.3s ease;


`;

export const Ar = styled.div`

background-image:url(${Arrow});
background-size:contain;
width:20px;
height:20px;
background-repeat:no-repeat;
background-position:center;
-webkit-transform: rotate(90deg);
transform:  ${props => props.open ? "rotate(0deg)" : "rotate(90deg)"};
float:right;
transition: all 0.3s ease-out;
`
export const ProfileList = styled.ul`
list-style-type:none;
padding:0;
display:flex-block;
flex:1;
width:98%

&>li{
  border-radius:10px;
  background-color:#f9f9f9f2;
  margin:10px 0;
  padding:20px;
  text-align:left;
  display:flex;
  flex:1;
  flex-direction:column;
  cursor:pointer;

  :hover{
    background-color:#ffff;
  }
}
`

export const HeadStyle = styled.div`
  background-image:url(${headBg});
  background-repeat:no-repeat;
  background-size:cover;
  background-position:center;
  border-radius:10px;
  height:100px;
  flex-direction:row;
  width:98%;
  margin:10px 0 ;
  justify-content:center;
  align-item:center;


  &>span{
    font-size:20px;
    color:#fff;
    display:flex;
    margin:0 auto;
    flex:1;
  }
  `

export const Main = styled.div`
    text-align: center;
    display: flex;
    flex-direction:column;
    flex:1;
    justify-content: center;
      align-items: center;
   
`
export const BreadCrumb = styled.div`
  padding:10px;
  text-align:left;
  font-size:16;
  font-color:#ccc;
  font-weight:bold;
  background-color:#ddf4f7;
  margin:20px auto;
  border-radius:10px;

`


export const CardLeft = styled.div`
    font-family: Montserrat;
    width:300px;
    background:orange;
    padding:10px
    height:190px;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    float:left;
    margin:20px;


    .head {
      font-size:18px;
      font-weight:bold;
      color:white;
    }

    & > div {
      & > div{
        padding-top:20px;
        padding-right:10px;
        font-size:75px;  
        font-weight:600;
        color:white;
      }
    } 
    }
    `

    export const CardRight = styled.div`
    font-family: Montserrat;
    width:300px;
    background:orange;
    padding:10px
    height:190px;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    float:right;
    margin:20px;

    .head {
      font-size:18px;
      font-weight:bold;
      color:white;
    }

    & > div {
      & > div{
        padding-top:20px;
        padding-right:10px;
        font-size:75px;  
        font-weight:600;
        color:white;
      }
    } 
    }
    `


 export const UsersContainer = styled.section`
 
 padding:20px;

 clear:both;
 margin-top:10px;
 `   

const btn = (light, dark) => css`
  white-space: nowrap;
  display: inline-block;
  border-radius: 5px;
  padding: 5px 15px;
  font-size: 16px;
  color: white;
  &:visited {
    color: white;
  }
  background-image: linear-gradient(${light}, ${dark});
  border: 1px solid ${dark};
  &:hover {
    background-image: linear-gradient(${light}, ${dark});
    &[disabled] {
      background-image: linear-gradient(${light}, ${dark});
    }
  }
  &:visited {
    color: black;
  }
  &[disabled] {
    opacity: 0.6;
    cursor: not-allowed;
  }
`

const btnDefault = css`
  ${btn('#ffffff', '#d5d5d5')} color: #555;
`

const btnPrimary = btn('#4f93ce', '#285f8f')
const btnDanger = btn('#e27c79', '#c9302c')

export default styled.div`
  font-family: sans-serif;
  overflow:auto;
  clear:both;
  h1 {
    text-align: center;
    color: #222;
  }

  h2 {
    text-align: center;
    color: #222;
  }

  & > div {
    text-align: center;
    width:800px;
    margin:0px auto;

    
  }

  a {
    display: block;
    text-align: center;
    color: #222;
    margin-bottom: 10px;
  }

  p {
    max-width: 500px;
    margin: 10px auto;
    & > a {
      display: inline;
    }
  }

  .left{
    float:left;
  }
  
  .right{
    float:right;
  }

  form {
    max-width: 500px;
    margin: 10px auto;
    border: 1px solid #ccc;
    padding: 20px;
    box-shadow: 2px 2px 5px rgba(0, 0, 0, 0.3);
    border-radius: 3px;
    position: relative;

    .loading {
      text-align: center;
      display: block;
      position: absolute;
      background: url('https://media.giphy.com/media/130AxGoOaR6t0I/giphy.gif')
        center center;
      background-size: fill;
      font-size: 2em;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      padding: 50px 0 0 0;
      z-index: 2;
    }

    & > div {
      display: flex;
      flex-flow: row nowrap;
      line-height: 2em;
      margin: 5px;
      position: relative;
      & > label {
        color: #333;
        width: 110px;
        min-width: 60px;
        font-size: 1em;
        line-height: 32px;
      }
      
      & > input,
      & > select,
      & > textarea {
        flex: 1;
        padding: 3px 5px;
        font-size: 1em;
        margin-left: 15px;
        border: 1px solid #ccc;
        border-radius: 3px;
      }
      & > input[type='checkbox'] {
        margin-top: 7px;
      }
      & > div {
        margin-left: 16px;
        & > label {
          display: block;
          & > input {
            margin-right: 3px;
          }
        }
      }
      & > span {
        line-height: 32px;
        margin-left: 10px;
        color: #800;
        font-weight: bold;
      }
      & > button.remove {
        ${btnDanger};
      }
    }
    & > .buttons {
      display: flex;
      flex-flow: row nowrap;
      justify-content: center;
      margin-top: 15px;
    }

    .error {
      display: flex;
      font-weight: bold;
      color: #800;
      flex-flow: row nowrap;
      justify-content: center;
    }
    pre {
      position: relative;
      border: 1px solid #ccc;
      background: rgba(0, 0, 0, 0.1);
      box-shadow: inset 1px 1px 3px rgba(0, 0, 0, 0.2);
      padding: 20px;
    }
  }
  button {
    margin: 0 10px;
    &[type='submit'] {
      ${btnPrimary};¸
    }
    &[type='button'] {
      ${btnDefault};
    }
  }
`
