import React, {
    Component
  } from 'react';
 
import Homenav from './homenav';
import About from './About';
import Main from './Main';
import Contact from './Contact';
  
  class Home extends Component {
    render() {
      return (
        <div className='container'>
          <Homenav />
            <Main className='child'/>
            <About className='child'/>
            <Contact className='child'/>
        </div>
      );
    }
  }
  
  export default Home;