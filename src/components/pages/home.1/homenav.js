import React, {
  } from 'react';

  import AppBar from '@material-ui/core/AppBar';
  import Toolbar from '@material-ui/core/Toolbar';
  import Button from '@material-ui/core/Button';
  import { withStyles } from '@material-ui/core/styles';
  import ReactPageScroller from "react-page-scroller";
  import './mystyle.css';
 

  const goToPage = (pageNumber) => {
        this.reactPageScroller.goToPage(pageNumber);
    }

  const styles = theme => ({
    button: {
      margin: theme.spacing.unit,
    },
    input: {
      display: 'none',
    },
    
  });

  let last_known_scroll_position = 0;

  const findPos = (obj)=> {

    console.log(window.innerHeight)
    last_known_scroll_position = window.scrollY;
    console.log('last: ', last_known_scroll_position);
    return window.outerHeight;
}




    const onPress = (e,k)=>{
      console.log('impress the ',e )
      console.log('k',(window.outerHeight*k));
      //window.scroll(0,findPos(500));
      //scrollTo(window);
      let to = window.outerHeight*k;
      window.scrollTo({ top: (to), behavior: 'smooth' })
     
    }

    const buttons = ['home','about','services','contact us'];

    const renderBtn = buttons.map((data,key)=>{
        return(
        <Button className='fr' key={key} onClick={()=>{onPress(data,key)}}>{data}</Button>
        )
    })


    const Homenav = props => {
        const { classes } = props;
        return (
        <div className='btnright'>
        {renderBtn}
         </div>
        )
    }

  //  export default Homenav;
  export default withStyles(styles,{name:'Homenav'})(Homenav);
