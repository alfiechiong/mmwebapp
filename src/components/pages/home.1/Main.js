import React, {
    Component
  } from 'react';
  import './mystyle.css';
  
  class Main extends Component {
    render() {
      return (
        <div className='fullScreen mainbackground'>
            <h1> This is a jsx Main</h1>
        </div>
      );
    }
  }
  
  export default Main;