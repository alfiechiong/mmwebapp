import React,{Component} from 'react'
import Ag from '../../common/aggreement'
import {withRouter} from 'react-router-dom'
import {Agree} from './Styles'

class Agreement extends Component {

    goTo = ()=>{
        this.props.history.push('dashboard')
    }

    render(){
        return (
            <Agree><Ag/></Agree>
        )
    }
    
}

export default withRouter(Agreement);