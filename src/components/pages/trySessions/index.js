import React, {
    Component
  } from 'react';
  import {withRouter} from 'react-router-dom';
  import {
    Main,
    Calmness,
    Relaxation,
    Reset,
    Title
    } from './Styles';

    import {GetTrySession} from './functions'
  import {connect} from 'react-redux'

  const mapStateToProps = state => {
    return { 
      dbUsers: state.user.dbUsers[0],
      userCount:state.user.userCount
    };
  };

  class TrySession extends Component {

    constructor(props){
      super(props)

              
    

    }


    goTo = (url,cat)=>{
      this.props.history.push(url,{type:cat})
    }

    render() {
      
      return (
        
        <Main>  
            <Calmness onClick={()=>this.goTo('trysession/session','Calm')}><Title>Calmness</Title></Calmness>
            <Relaxation onClick={()=>this.goTo('trysession/session','Relax')}><Title>Relaxation</Title></Relaxation>
            <Reset onClick={()=>this.goTo('trysession/session','Reset')}><Title>Reset</Title></Reset>
        </Main>
       
      );
    }
  }
  export default connect(mapStateToProps)(withRouter(TrySession));
  //export default Dashboard;