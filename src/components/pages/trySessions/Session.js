import React, { Component } from 'react'
import AudioView from '../../common/audio/AudioView'
import {GetTrySession} from './functions'

class Session extends Component{

    state = {
        data:undefined
    }

    componentDidMount = async ()=>{
       // console.log(this.props.location.state.type)
//    /    console.log(this.getDocs(this.props.location.state.type))
    this.setState({data:await GetTrySession(this.getDocs(this.props.location.state.type))})
    }
    
    getDocs = (cat)=>{
        switch(cat) {
            case 'Calm':

            return 'kNHjBNNgystCzTKnc35g'

            case 'Relax':

            return 'wCeyLMA3GKD5gzMQ3nNW'

            case 'Reset':

            return 'YWfOjhqNgV2xHZWxII2h'

            default: return ''
        }

    }

    render(){
        return (
            
            <div>
                <AudioView data={this.state.data && this.state.data}/>
            </div>
        )
    }
}

export default Session