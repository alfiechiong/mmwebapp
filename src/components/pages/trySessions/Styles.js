import styled, { css } from 'styled-components'
import calmness from '../../../assets/calmness.jpg'
import relaxation from '../../../assets/relaxation.jpg'
import reset from '../../../assets/reset.jpg'

export const Main = styled.div`
height: 100vh;;
display:flex;
flex-direction:column;
flex:1;

@media(max-width: 400px) {
  min-width:400px;
  max-width:400px;
}

@media(max-width: 360px) {
  min-width:300px;
  max-width:300px;
}

@media(max-width: 411px) {
  min-width:400px;
  max-width:400px;
}
    text-align: center;
    
    margin:0px auto;
    overflow:auto;
    clear:both;

    img{
      width:100px;
      margin:30px 10px 10px 10px;
    }
`

export const Title = styled.div`
margin-top:25px;
background-color:rgba(243, 243, 243, 0.6);
width:300px;
padding:10px;
height:30px;
font-size:30px;

`

export const Calmness = styled.div`
background:url(${calmness});
display:flex;
flex:1;
background-size:cover;
background-repeat:no-repeat;
background-position:center;
align-item:center;
cursor:pointer;

:hover{
  opacity: 0.7;
}
`

export const Reset = styled.div`
background:url(${reset});
display:flex;
flex:1;
background-size:cover;
background-repeat:no-repeat;
background-position:center;
align-item:center;
cursor:pointer;

:hover{
  opacity: 0.7;
}
`

export const Relaxation = styled.div`
background:url(${relaxation});
background-size:cover;
background-repeat:no-repeat;
background-position:middle bottom;
display:flex;
flex:1;
align-item:center;
cursor:pointer;

:hover{
  opacity: 0.7;
}
`