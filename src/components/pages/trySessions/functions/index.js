import React from 'react';
import {db} from '../../../../firebase'

const renderUser = (item) =>{
    return (
      <tr>
      <td> {item}</td>
      </tr>
    )
  }

export const GetAllUsers = () =>{
   return new Promise ((res,rej)=>{
        db.collection("Users")
            .onSnapshot((doc) => {
            // console.log("Current data: ", doc.docChanges());
                doc.docChanges().map((change) => {
                    if (change.type === "added") {
                        console.log("New User: ", change.doc.data().name);
                        //return renderUser(change.doc.data())
                        res(change.doc.data().name)
                    }
                    if (change.type === "modified") {
                        console.log("Modified User : ", change.doc.data());
                        res(change.doc.data().name)
                    }
                    if (change.type === "removed") {
                        console.log("Removed User: ", change.doc.data());
                        res(change.doc.data());
                    }
                });
            }); 
    })
}

export const GetTrySession = async (docid)=>{
    const ref = db.collection('Sessions').doc(docid).get();

    return ref.then(doc=>{
        console.log(doc.data())
        return doc.data()
    }).catch(err=>console.log(err))
}