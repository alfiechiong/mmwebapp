import React, {
    Component
  } from 'react';

  import {BreadCrumb ,Main} from './Styles';

  import {Elements, StripeProvider} from 'react-stripe-elements';
  import StripeCheckout from '../../common/StripeCheckout'

  class BeAMember extends Component {

    state = {
      type:undefined
    }
    constructor(props){
      super(props)
    }

    handleRadio = (val)=>{
      this.setState({type:val})
      console.log(val)
    }

    render() {
      
      return (
        
        <Main>  
        <BreadCrumb >Subscription</BreadCrumb>
      
        <StripeProvider apiKey="pk_test_M2aqextQa5rvOyVZuNKIvF6P">
        <div className="example">
          <Elements>
            <StripeCheckout />
          </Elements>
        </div>
      </StripeProvider>

     
       </Main>
       
      );
    }
  }
  export default BeAMember;
  //export default Dashboard;