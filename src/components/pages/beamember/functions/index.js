import React from 'react';
import {db} from '../../../../firebase'
import axios from 'axios'

const renderUser = (item) =>{
    return (
      <tr>
      <td> {item}</td>
      </tr>
    )
  }

export const GetAllUsers = () =>{
   return new Promise ((res,rej)=>{
        db.collection("Users")
            .onSnapshot((doc) => {
            // console.log("Current data: ", doc.docChanges());
                doc.docChanges().map((change) => {
                    if (change.type === "added") {
                        console.log("New User: ", change.doc.data().name);
                        //return renderUser(change.doc.data())
                        res(change.doc.data().name)
                    }
                    if (change.type === "modified") {
                        console.log("Modified User : ", change.doc.data());
                        res(change.doc.data().name)
                    }
                    if (change.type === "removed") {
                        console.log("Removed User: ", change.doc.data());
                        res(change.doc.data());
                    }
                });
            }); 
    })
}

export const stripeCharge = () =>{
    return axios.post('https://us-central1-mindsetmaestro-c57eb.cloudfunctions.net/stripeCharge').then((response) => {
      /* this.setState({
        buyItems: response.data
      }) */
      console.log(response.data);
    })
}