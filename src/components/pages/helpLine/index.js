import iplocation from "iplocation";
import React,{ Component } from "react";
import {getHelpLine} from '../../common/utils/getHelpLine'
import {Style} from './Styles'

const publicIp = require('public-ip');

class HelpLine extends Component {

    constructor(props){
        super(props)

        this.state = {
            name:'',
            country:'',
            phone:''
        }
    }

    componentDidMount = async()=>{
        const helpline = await getHelpLine(await this.getCountry());

        console.log('--',helpline.docs[0].data())

        this.setState({
            name:helpline.docs[0].data().fullname,
            country:helpline.docs[0].data().country,
            phone:helpline.docs[0].data().phone
        })
    }

    getCountry = async () => {

        const ip = await publicIp.v4();
        console.log('ip4: ', ip);
    
        //console.log('iplocation: ', iplocation(ip))
          
        return iplocation(ip).then(val=>{
          console.log('return: ',val)
          return val.country
        });
        //=> '46.5.21.123'
     
       // console.log('ip6: ',await publicIp.v6());
        //=> 'fe80::200:f8ff:fe21:67cf'
    }

    render(){
        return(
            <Style>
            <h1>Helpline</h1>

            <ul>
                <li><h3>Name:</h3> <span>{this.state.name && this.state.name}</span> </li>
                <li><h3>Country</h3> <span>{this.state.country && this.state.country}</span> </li>
                <li><h3>Number:</h3> <span>{this.state.phone && this.state.phone}</span> </li>

            </ul>
            </Style>
        )
    }

}

export default HelpLine