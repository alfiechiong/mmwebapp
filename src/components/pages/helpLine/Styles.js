import styled from 'styled-components'

export const Style = styled.div`

margin:20px auto;
display:flex;
align-item:center;
justify-content:center

max-width:500px;
padding:20px;

border:solid thin #ccc;
border-radius:10px;
flex-direction:column;

background-color:#fff;

h1{
    display:flex;
    flex:1;
    padding:10px;
    text-align:center;
    border:solid thin #ccc;

}
ul{
    display:flex;
    flex-direction:column
    flex:1;
    list-style:none;

}

li{
flex:1
text-align:left;
}

span{
    padding:10px;
    background-color:#d8e7ef;
    border-radius:7px;
}

&>h3{
    flex:1
}

`