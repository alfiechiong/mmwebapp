import React from 'react';  
import { Route,Redirect } from 'react-router-dom';
import {Consumer} from '../AppProvider';
import Login from '../Login';

// Utils
//import auth from '../../utils/auth';

const PrivateRoute = (props) => (  
    <Consumer>
    {
      ({ state }) => 
      
     // state.email && console.log(state.email)

       state.currentUser !== null ? 
      <Route exact path={props.path} component={props.component} />
      :
      <Redirect to='/' />
    }
  </Consumer> 
); 

export default PrivateRoute;