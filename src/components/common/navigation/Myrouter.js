import React, {
    Component,
    Fragment
  } from 'react';
  import {
    BrowserRouter as Router,
    Route,
    Link,
  } from 'react-router-dom';
  
  import PrivateRoute from '../PrivateRoute';
  import SubscriberRoute from '../SubscribersRoute'
  import Login from '../../Login';
  import Signup from '../../Signup'
  import FlashMessage from '../../../shared/FlashMessage'
  import Dashboard from '../../pages/dashboard'
  import Me from '../../pages/meme'
  import Content from '../../pages/content'
  import AddSessions from '../../pages/content/AddSessions'
  import AddCourses from '../../pages/content/AddCourses'
  import Settings from '../../pages/settings'
  import Nav from './DashboardNav'
  import {Container, PageTitle} from './Styles'
  import { Consumer } from '../../AppProvider'
  import BeAMember from '../../pages/beamember'
  import Agreement from '../../pages/agreement'
  import Logout from '../../Logout'
  import FreeSession from '../../pages/freeSessions'
  import OneMinute from '../../pages/oneMinute'
  import UserCourses from '../../pages/UserCourses'
  import CourseSessions from '../../pages/UserCourses/CourseSessions'
  import CustomCourseSessions from '../../pages/UserCourses/CustomCourseSessions'
  import PowerUps from  '../../pages/powerUps'
  import HelpLine from '../../pages/helpLine'
  import CustomCourses from '../../pages/CustomCourses'
  import CheckIn from '../../pages/checkIn'
  import TrySession from '../../pages/trySessions'
  import SessionRand from '../../pages/trySessions/Session'
  import Profile from '../../pages/Profile'

  import HomeIcon from '../../../assets/homebig.svg'
  import ContentIcon from '../../../assets/contentbig.svg'
  import SettingsIcon from '../../../assets/settingsbig.svg'
  import {SideBar} from './SidebarNav';
  
  class Myrouter extends Component {

    getIcon = (loc)=>{
      switch (loc){
        case 'dashboard':
            return HomeIcon;
        case 'content':
            return ContentIcon;
        case 'settings':
            return SettingsIcon;
        default:
          return HomeIcon 
      }
    }

    render() {
      return (
          <Router>
            <Fragment>
              <SideBar hide={true}/>
              <Nav />
            
              <Container>
              <FlashMessage />
              <Route exact path="/" component={Login} />
              <Route exact path="/login" component={() => <Login />} />
              <Route exact path="/signup" component={() => <Signup />} />
             
              <PrivateRoute exact path="/profile" component={Profile} />
              <PrivateRoute exact path='/dashboard' component={Dashboard} />
              <PrivateRoute exact path='/content' component={Content} />
              <PrivateRoute exact path='/addsessions' component={AddSessions} />
              <PrivateRoute exact path='/addcourses' component={AddCourses} />
              <PrivateRoute exact path='/settings' component={Settings} />
              <PrivateRoute exact path='/beamember' component={BeAMember} />
              <PrivateRoute exact path='/me' component={Me} />
              <PrivateRoute exact path='/freesessions' component={FreeSession} />
              <PrivateRoute exact path='/oneminute' component={TrySession} />
              <PrivateRoute exact path='/powerups' component={PowerUps} />
              <PrivateRoute exact path='/helpline' component={HelpLine} />
              <PrivateRoute exact path='/customcoursesessions' component={CustomCourseSessions} />
              <PrivateRoute exact path='/checkin' component={CheckIn} />
              <PrivateRoute exact path='/trysession' component={OneMinute} />
              <PrivateRoute exact path='/trysession/session' component={SessionRand} />
              <PrivateRoute exact path='/usercourses' component={UserCourses} />
              <PrivateRoute exact path='/coursesessions' component={CourseSessions} />
              <PrivateRoute exact path='/customcourses' component={CustomCourses} />
              {/* <Route exact path="/dashboard" component={() => <Consumer>
                {
                  ({ state }) => state.currentUser !== null ?
                  <Dashboard />:<Link to='/signup' > Please Create Your Account First </Link>
                }
              </Consumer>} /> */}
               
              <Route exact path="/signedOut" component={()=><Logout />} />
              <Route exact path="/agreement" component={() => <Agreement />} />
              <Route exact path="/accountCreated" component={() => 
                <h1 className="content">Account created. <Link to="/login">
                Proceed to Dashboard</Link></h1>} />
            </Container>

            </Fragment>
           
       
          </Router>
      );
    }
  }
  
  export default Myrouter;
  