import styled, { css } from 'styled-components'
import {media} from '../../common/media'

export const PageTitle = styled.div`
  height:30px;
  clear:both;
  padding:20px 0 20px 20px;
  text-align:left;
  font-size:30px;
  font-weight:600;
  
  background-color:#f1f1f1;

  &span > {
  
  float:left;
  }
  
  &img >{
    float:left;
  }
`


export const NavBar = styled.div`
    width:100vw;
    top:0px;
    border-bottom:solid thin #ccc
    list-style-type: none;
    margin: 0px;
    background-color:#2d3c4d;
    height:53px;

    &>div {
      display:block;
      float:left;
    }

    ul{
      margin:0;
    }

    .left{
      float:left;
      width:30%;
      @media(max-width: 360px) {
        width:100%;
        float:none;
        text-align:center;
        padding-top:5px;
        margin-bottom:10px;
      }
      
   
      
        ${media.tablet`
        font-size:12px;
        `
        }
  
      ${media.phone` 
      font-size:10px;
      `}
    }

    .right{
      float:right;
      text-align:right;
      padding-right:20px;
      width:65%;
  
    }
`

export const Logo = styled.div`
    width:100%;
    margin-top:10px;
    padding: 5px;
    color:#ccc

    @media (max-width: 400px) {
      font-size:12px;
    }

    @media(max-width: 360px) {
      font-size:10px;
    }
    
    @media(max-width: 411px) {
      font-size:12px;
    }
}`

export const Nav = styled.ul`

    width:100%;
    padding:0;
    img{
      float:left;
      margin-top:0;
      margin-right:0;
    }
    li{
      padding:0;
        display: inline-block;
        text-align:left;
    }
    span,a{


  
${media.tablet`  visibility:hidden;
display: none;`
}
  
${media.phone`  
visibility:hidden;
display: none;`
}
    
        margin-top:15px;
        display: block;
        color:#ccc;
        text-decoration:none;
        float:left;
        cursor:pointer;
    }
`


export const Container = styled.div`

& > div {
  text-align: center;
}
`

const btn = (light, dark) => css`
  white-space: nowrap;
  display: inline-block;
  border-radius: 5px;
  padding: 5px 15px;
  font-size: 16px;
  color: white;
  &:visited {
    color: white;
  }
  background-image: linear-gradient(${light}, ${dark});
  border: 1px solid ${dark};
  &:hover {
    background-image: linear-gradient(${light}, ${dark});
    &[disabled] {
      background-image: linear-gradient(${light}, ${dark});
    }
  }
  &:visited {
    color: black;
  }
  &[disabled] {
    opacity: 0.6;
    cursor: not-allowed;
  }
`

const btnDefault = css`
  ${btn('#ffffff', '#d5d5d5')} color: #555;
`

const btnPrimary = btn('#4f93ce', '#285f8f')
const btnDanger = btn('#e27c79', '#c9302c')

export default styled.div`
  font-family: sans-serif;

  h1 {
    text-align: center;
    color: #222;
  }

  h2 {
    text-align: center;
    color: #222;
  }

  & > div {
    text-align: center;
  }

  a {
    display: block;
    text-align: center;
    color: #222;
    margin-bottom: 10px;
  }

  p {
    max-width: 500px;
    margin: 10px auto;
    & > a {
      display: inline;
    }
  }

  
  form {
    max-width: 500px;
    margin: 10px auto;
    border: 1px solid #ccc;
    padding: 20px;
    box-shadow: 2px 2px 5px rgba(0, 0, 0, 0.3);
    border-radius: 3px;
    position: relative;

    .loading {
      text-align: center;
      display: block;
      position: absolute;
      background: url('https://media.giphy.com/media/130AxGoOaR6t0I/giphy.gif')
        center center;
      background-size: fill;
      font-size: 2em;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      padding: 50px 0 0 0;
      z-index: 2;
    }

    & > div {
      display: flex;
      flex-flow: row nowrap;
      line-height: 2em;
      margin: 5px;
      position: relative;
      & > label {
        color: #333;
        width: 110px;
        min-width: 60px;
        font-size: 1em;
        line-height: 32px;
      }
      & > input,
      & > select,
      & > textarea {
        flex: 1;
        padding: 3px 5px;
        font-size: 1em;
        margin-left: 15px;
        border: 1px solid #ccc;
        border-radius: 3px;
      }
      & > input[type='checkbox'] {
        margin-top: 7px;
      }
      & > div {
        margin-left: 16px;
        & > label {
          display: block;
          & > input {
            margin-right: 3px;
          }
        }
      }
      & > span {
        line-height: 32px;
        margin-left: 10px;
        color: #800;
        font-weight: bold;
      }
      & > button.remove {
        ${btnDanger};
      }
    }
    & > .buttons {
      display: flex;
      flex-flow: row nowrap;
      justify-content: center;
      margin-top: 15px;
    }

    .error {
      display: flex;
      font-weight: bold;
      color: #800;
      flex-flow: row nowrap;
      justify-content: center;
    }
    pre {
      position: relative;
      border: 1px solid #ccc;
      background: rgba(0, 0, 0, 0.1);
      box-shadow: inset 1px 1px 3px rgba(0, 0, 0, 0.2);
      padding: 20px;
    }
  }
  button {
    margin: 0 10px;
    &[type='submit'] {
      ${btnPrimary};
    }
    &[type='button'] {
      ${btnDefault};
    }
  }
`
export const SidebarStyle = styled.div`
width:300px;
height:100%;
position:fixed;
top:0;
right:0;
background:#8b4622eb;


padding:20px;
ul{
  display:flex;
  flex-direction:column;
  flex:1;
  align-item:center;
  justify-contenct:center;
  list-style:none;
  
}

li{
  padding:10px;
  border-bottom:solid thin #ccc;
  flex:1
  color:white;

}
`