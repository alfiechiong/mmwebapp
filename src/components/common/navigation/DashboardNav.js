import React from 'react';
import {
  withRouter
} from 'react-router-dom';
import { auth } from '../../../firebase';
import { Consumer } from '../../AppProvider';
import {NavBar,Nav, Logo} from './Styles';
import HomeIcon from '../../../assets/home.svg'
import ContentIcon from '../../../assets/content.svg'
import ProfileIcon from '../../../assets/Profile.svg'
import SettingsIcon from '../../../assets/settings.svg'
import LogoutIcon from '../../../assets/logout.svg'
import {PageTitle} from './Styles'
import navLogo from '../../../assets/navLog.png'


const Navbar = props => {

  

  const handleLogout = context => {
    auth.logout();
    context.destroySession();
    props.history.push('/signedOut');
  };

  const SetToContent = (context,page) =>{
    context.setCurrentLocation(page);
    props.history.push('/'+page);
  }



const getIcon = (loc)=>{
    switch (loc){
      case 'dashboard':
          return HomeIcon;
      case 'content':
          return ContentIcon;
      case 'settings':
          return SettingsIcon;
      default:
        return HomeIcon 

    }
  }

  return <Consumer>
    {({ state, ...context }) => (
      state.currentUser ?
        <NavBar>
              <div className='left'>  
        <Logo><img src={navLogo} height='30'/></Logo>
        </div>
        <div className='right'>
        <Nav>
        <li onClick={()=>SetToContent(context,'dashboard')}><img src={HomeIcon} width='30' height='50'/><span>Dashboard</span></li>
       
        {(state.type === 'subscriber'  ? <li onClick={()=>SetToContent(context,'profile')}><img src={ContentIcon} width='50' height='50'/><span>Profile</span></li>:'')} 

       {/*  <li onClick={()=>SetToContent(context,'content')}><img src={ContentIcon} width='50' height='50'/><span>Content</span></li>
        <li onClick={()=>SetToContent(context,'settings')}><img src={SettingsIcon} width='50' height='50'/><span>Settings</span></li> */}
         <li onClick={() => handleLogout(context)}><img src={LogoutIcon} width='50' height='50'/><span>Logout</span></li>
        </Nav>
        </div>
        {({ state}) => (
                  <PageTitle>
                   <img src={getIcon(state.currentLocation)} width='70' height='50'/><span>{state.currentLocation} - Page</span>
                  </PageTitle>
                )}
        </NavBar>
        :
        ''
    )}
  </Consumer>
};

export default withRouter(Navbar);