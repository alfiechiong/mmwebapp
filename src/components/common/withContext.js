import React from 'react'
import {Consumer} from '../AppProvider'

export const withConsumer = Comp => props => (
    <Consumer>
      {(state,...context) => <Comp context={context} state={state} />}
    </Consumer>
  );