import React ,{ Fragment}from 'react'
import classNames from 'classnames'
import ReactDropzone from 'react-dropzone'
import {Audioplayer} from '../audioplayer';
import {Consumer} from '../../AppProvider';
import {Field, Form} from 'react-final-form';
import {Required} from '../../validator';
import Styles,{Dropzone,UploadedSessionlist,SubmitBtn} from './Styles'
import FileUploader from "react-firebase-file-uploader";
import {storage} from '../../../firebase';
import arrayMutators from 'final-form-arrays'
import { FieldArray } from 'react-final-form-arrays'
import { isEmpty } from 'react-redux-firebase'
import {setDroppedFiles,updateDroppedFiles} from '../../../redux/actions';
import {connect} from 'react-redux';
import DropToUpload from 'react-drop-to-upload';
import {Add} from './functions'
import firebase from 'firebase'
import { Line, Circle } from 'rc-progress';
import {
  withRouter
} from 'react-router-dom';

const mapDispatchToProps = dispatch => {
    return {
    setDroppedFiles: files => dispatch(setDroppedFiles(files)),
    updateDroppedFiles: files => dispatch(updateDroppedFiles(files)),
    };
  };

  const mapStateToProps = state => {
    return { dropped: state.droppedFiles.droppedFiles
       
    };
  };

class MyDropzone extends React.Component {
    constructor(props) {
        super(props);
     
        this.handleDrop = this.handleDrop.bind(this);
        this.handleDropArrayBuffer = this.handleDropArrayBuffer.bind(this);
        this.handleDropDataURI = this.handleDropDataURI.bind(this);

        this.state = {
            filename:'',
            droppedfiles:[],
            progress:undefined,
            dropcount:0,
            drophide:false,
            drophover:false
        }
      }
     
      handleDrop = async (files) =>{
        console.log(files.length > 0); // true
        this.setState({dropcount:this.state.dropcount+files.length, drophide:true})
        console.log(files[0] instanceof File); // true

        //this.setTable(files)
        //console.log(files);
       
        this.uploadFiles(files);
        
      }
     
      handleDropArrayBuffer(arrayBuffers, files) {
        console.log(files.length > 0); // true
        console.log(files.length === arrayBuffers.length); // true
        console.log(files[0] instanceof File); // true
        console.log(arrayBuffers[0] instanceof ArrayBuffer); // true
      }
     
      handleDropDataURI(dataURIs, files) {
        console.log(files.length > 0); // true
        console.log(files.length === dataURIs.length); // true
        console.log(files[0] instanceof File); // true
        console.log(typeof dataURIs[0] === 'string'); // true
        console.log(/^data:(.*);(.*),/.test(dataURIs[0])); // true
      }

  
      uploadFiles = (files) =>{
        var data = new FormData();

 
        files.forEach((file, index) => {

        
       
            // Create a root reference
    const upload = storage.ref('audio').child(file.name).put(file);



// Register three observers:
// 1. 'state_changed' observer, called any time the state changes
// 2. Error observer, called on failure
// 3. Completion observer, called on successful completion
upload.on('state_changed', (snapshot) =>{
    // Observe state change events such as progress, pause, and resume
    // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
    var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
    this.setState({['progress'+index]:progress});
    console.log('Upload is ' + progress + '% done' );
    switch (snapshot.state) {
      case firebase.storage.TaskState.PAUSED: // or 'paused'
        console.log('Upload is paused');
        break;
      case firebase.storage.TaskState.RUNNING: // or 'running'
        console.log('Upload is running');
        break;
    }
  }, (error) => {
    // Handle unsuccessful uploads
    // A full list of error codes is available at
  // https://firebase.google.com/docs/storage/web/handle-errors
  switch (error.code) {
    case 'storage/unauthorized':
      // User doesn't have permission to access the object
      window.alert('Permission denied to upload')
      break;

    case 'storage/canceled':
      // User canceled the upload
      window.alert('Upload Canceled')
      break;

    case 'storage/unknown':
      // Unknown error occurred, inspect error.serverResponse
      window.alert('found error ')
      break;
  }
  }, () => {
    // Handle successful uploads on complete
    // For instance, get the download URL: https://firebasestorage.googleapis.com/...
    upload.snapshot.ref.getDownloadURL().then((downloadURL) => {
      console.log('File available at', downloadURL);

      this.props.setDroppedFiles({
        "filename":file.name,
        "filepath":downloadURL
    })
    });
  });
        });
     }

      handleUploadSuccess = (filename) => {
        // this.setState({ avatar: filename, progress: 100, isUploading: false });
         storage
           .ref("audio")
           .child(filename)
           .getDownloadURL()
           .then(
               url =>
                 {
                     //console.log(this.context);
                    
                      console.log(url)
                    
                 }
               );
       };

       mapToInput = () =>{
           
           const myuploaded = this.props.dropped.map((item,key)=>{
           /*  this.setState({droppedFiles:[this.state.droppedfiles, 
                {'name':item.filename,
                'description':'defaultDescriptiuon',
                'path':item.filepath
            }]}) */
                return this.generateOutput(item, key)
               //this.generateTable(item, key)

            })

           return myuploaded;
       }

       mapRow = () =>{
           return this.generateTable();
       }
       updateInput = e => {
        this.setState({
          [e.target.name]: e.target.value
        });
      }

  

       generateOutput = (item,k) =>{

           return(
            <tr key={k}>

           <td> <input
            type="text"
            name={"filename"+k}
            ref={"filename"+k}
            placeholder="Name"
            onChange={this.updateInput}
            defaultValue={item.filename}
          /></td>
          <td>
            <textarea name={"description"+k} ref={"desc"+k} defaultValue={item.filename}></textarea>
        </td>
        <td>

        <Audioplayer track={item.filepath} />
        <input type='text' name={'path'+k} ref={'path'+k} hidden defaultValue={item.filepath} />
          </td>
                    </tr>
           )       
        }
     
           handleSubmit = async (e) => {
              e.preventDefault();
               console.log('count', this.props.dropped.length)
               console.log('state', await this.state)
               this.setToState()
               
         
          }

          setToState = () =>{
              const count = this.props.dropped.length;

              const data = [];
              for(let x=0; x < count; x++) {
                  const filename = this.refs['filename'+x].value
                  const description = this.refs['desc'+x].value
                  const path = this.refs['path'+x].value
                  //console.log(node);
               data.push(
                   {name:filename,
                    description:description,
                    path:path
                })

                Add({name:filename,
                    description:description,
                    path:path
                })

                //window.location.reload(); 
                

              }
              this.props.history.push('/content');

             
          }

    
          generateProgress =  () =>{
                  
            var indents = [];
                for (var i = 0; i < this.state.dropcount; i++) {
                  indents.push(this.state['progress'+i] &&<Line percent={this.state['progress'+i]}
                    strokeWidth="1" strokeColor="#333" key={i} />);

                

                for (var J = 0; J < this.state.dropcount; J++) {
                if(this.state['progress'+J] == 100){
                    indents.splice(J, 1)
                    }
                }

              
            }

         
            return indents
               
          }
       
        hoverDrop = () =>{
            this.setState({drophover:true})
        }

        hoverOut = () =>{
            this.setState({drophover:false})
        }


      render() {

   

        return (
        <div>
            <Styles>
          <DropToUpload
            onDrop={ this.handleDrop }
            onDropArrayBuffer={ this.handleDropArrayBuffer }
            onDropDataURI={ this.handleDropDataURI }
            onOver={this.hoverDrop}
            onLeave={this.hoverOut}
          ><Dropzone hidden={this.state.drophide} hover={this.state.drophover}>
            Drop file here to upload
            </Dropzone>
          </DropToUpload>
          <form onSubmit={this.handleSubmit}>
          


           {
               this.generateProgress()
           }
          <UploadedSessionlist>
              <tbody>
                  <tr>
                      <th>Name</th>
                      <th>Description</th>
                      <th>Session</th>
                   </tr>
        {this.mapToInput()}
    
          </tbody>
          </UploadedSessionlist>
          <SubmitBtn>Submit</SubmitBtn>
          </form>
          </Styles>
          </div>
        );
      }
    }

export default connect(mapStateToProps,mapDispatchToProps)(withRouter(MyDropzone));