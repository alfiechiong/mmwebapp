import React, { Component } from "react";
//import firebase from "firebase";
import {storage} from '../../../firebase';
import FileUploader from "react-firebase-file-uploader";
import {Audioplayer} from '../audioplayer';
import {Consumer} from '../../AppProvider';
 
class Uploader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            avatar: "",
            isUploading: false,
            progress: 0,
            avatarURL: ""
          };
     
      }
  
 
 
  handleChangeUsername = event =>
    this.setState({ username: event.target.value });
  handleUploadStart = () => this.setState({ isUploading: true, progress: 0 });
  handleProgress = progress => this.setState({ progress });
  handleUploadError = error => {
    this.setState({ isUploading: false });
    console.error(error);
  };
  handleUploadSuccess = filename => {
   // this.setState({ avatar: filename, progress: 100, isUploading: false });
    storage
      .ref("audio")
      .child(filename)
      .getDownloadURL()
      .then(
          url =>
            {
                
                //console.log(this.context);
               // this.context.setUploadedSessionUrl(url);
               //  console.log(url)
                this.setState({ avatarURL: url,
                    avatar: filename, progress: 100, isUploading: false 
                 })  
                
            }
          );
  };
 
  render() {
    return (
        <Consumer>
             {({ state, ...context }) => (
                
      <div>
        <form>
          <label>Username:</label>
          <input
            type="text"
            value={this.state.username}
            name="username"
            onChange={this.handleChangeUsername}
          />
          <label>Avatar:</label>
          {this.state.isUploading && <p>Progress: {this.state.progress}</p>}
          {/* this.state.avatarURL && <img src={this.state.avatarURL} /> */ }
          {this.state.avatarURL  && <Audioplayer track={this.state.avatarURL} />}
         {/* this.state.avatarURL && context.setUploadedSessionUrl(this.state.avatarURL) */}
          <FileUploader
            accept="audio/*"
            name="avatar"
            //filename={file => this.state.username + file.name.split('.')[1]}
            randomizeFilename
            storageRef={storage.ref("audio")}
            onUploadStart={this.handleUploadStart}
            onUploadError={this.handleUploadError}
            onUploadSuccess={this.handleUploadSuccess}
            onProgress={this.handleProgress}
          />
        </form>
      </div>
             )}
      </Consumer>
    );
  }
}
 
export default Uploader;