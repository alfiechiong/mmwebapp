import React from 'react';
import Logo from '../../assets/bigLogo.png'
import {ImgLogo} from './Styles'
export const BigLogo = ()=>{
    return (
        <div>
            <ImgLogo src={Logo} />
        </div>    
    )
}