import styled from 'styled-components'

export const ModalHeader = styled.div`
  padding:10px;
  height:30px;
  background-color:#5fb9e2;
  text-align:center;
  font-size: 25px;
  font-weight: 600;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: left;
  color:white;

  & > span {
    float:right;
    width:30px;
    height:30px;
    text-align:center;
    cursor:pointer;
    font-weight:bold;
  }
`

export const Transparent = styled.div`
background: rgba(76, 175, 80, 0.3);
color:#ffff;
display:flex;
align-item:center;
justify-content:center;

&>section{
  height:200px;
  width:200px;
  margin:auto;
  align-item:center;
  justify-content:center;
}
`

export const SpinnerLoader = styled.div`
border: 16px solid #f3f3f3; /* Light grey */
border-top: 16px solid #3498db; /* Blue */
border-radius: 50%;
width: 120px;
height: 120px;
animation: spin 2s linear infinite;
margin:auto;


@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}


`



