import React from 'react';
import {ModalHeader} from './Styles';

const Modal = ({ handleClose, show, children,title }) => {
    const showHideClassName = show ? "modal display-block" : "modal display-none";
  
    return (
      <div className={showHideClassName}>
        <section className="modal-main">
        <ModalHeader>{title}  <span onClick={handleClose}>X</span></ModalHeader>
          {children}
    
        </section>
      </div>
    );
  };

  export {Modal}
  