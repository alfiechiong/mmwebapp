import React from 'react';
import {ModalHeader,SpinnerLoader,Transparent} from './Styles';

const Loader = ({ handleClose, show, children }) => {
    const showHideClassName = show ? "modal .display-flex" : "modal display-none";
  
    return (
      <Transparent className={showHideClassName}>
        <section>
        <span>Loading</span>
          <SpinnerLoader />
          <span onClick={handleClose}>X</span>
        </section>
      </Transparent>
    );
  };

  export {Loader}
  