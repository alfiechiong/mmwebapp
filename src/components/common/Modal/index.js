import React, { Component } from "react";
import ReactDOM from "react-dom";
import {Modal} from './modal'
import './style.css';
import { Header } from "../../childComponents/User/Styles";

class ModalIndex extends Component {
  state = { show: false };

  showModal = () => {
    this.setState({ show: true });
  };

  hideModal = () => {
    this.setState({ show: false });
  };

  render() {
    return (
      <main>
        <Header>React modal</Header>
        <Modal show={this.state.show} handleClose={this.hideModal}>
          <p>Modal</p>
         
        </Modal>
        <button type="button" onClick={this.showModal}>
          open
        </button>
      </main>
    );
  }
}

export default ModalIndex