import React from 'react';
import AudioPlayer from '../../common/audio/AudioPlayer';
import {ClearFix} from '../../common/ClearFIx'
import {Header,FreeSessionlist} from './Styles'
import {PowerUpgradeComp} from '../../common/powerUps'

class AudioView extends React.Component {

render() {
return (
  <div>
    <div>
       
    <FreeSessionlist>

    <Header>Change Your Perspective</Header>  
    <ul>
    <div> {this.props.data && this.props.data.name}</div>
    <li>
    
    <br />
    <br />
    <br />
    <br />
    <div> </div>

    </li>
    <AudioPlayer track={this.props.data && this.props.data.link} />
   
    <h3>Description</h3>
    <div>{this.props.data && this.props.data.description}</div>
    
  </ul>
    </FreeSessionlist>
    <PowerUpgradeComp />
  <ClearFix />
  </div>

  </div>
    );
  }
}

export default AudioView