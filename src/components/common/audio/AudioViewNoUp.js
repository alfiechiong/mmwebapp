import React from 'react';
import AudioPlayer from '../../common/audio/AudioPlayer';
import {ClearFix} from '../../common/ClearFIx'
import {Header,AudioViewN} from './Styles'
import {withConsumer} from '../../common/withContext'

class AudioView extends React.Component {

render() {
return (
  <div>
    <div>
       
    <AudioViewN>

    <Header>Change Your Perspective</Header>  
    <ul>
    <div> {this.props.data && this.props.data.name}</div>
    <li>
    
    <br />
    <br />
    <br />
    <br />
    <div> </div>

    </li>
    <AudioPlayer track={this.props.data && this.props.data.link}/>
   
    <h3>Description </h3>

    <div>{this.props.data && this.props.data.description}</div>
    
  </ul>
    </AudioViewN>
  <ClearFix />
  </div>

  </div>
    );
  }
}

export default AudioView