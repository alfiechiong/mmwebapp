import React, {Component} from "react";
import {PlayButton} from './Styles'

class AudioPlayer extends Component{

    state = {
        playing: false,
        button:'Play'
    }

    audio = new Audio;

    Play = ()=>{
    console.log("play")
    this.setState({ playing: true,button:'Stop' });
    this.audio.src = this.props.track;//this.state.songs[this.props.track];
    this.audio.play(); 
    }

    Pause = ()=>{
    console.log("Pause")
    this.setState({ playing: false,button:'Play' });
    this.audio.pause(); 
    }

    render(){
       
     return(
        <div>
         
            <PlayButton Playing={this.state.playing} onClick={this.state.playing ? this.Pause : this.Play}><p >{this.state.button}</p></PlayButton>
        </div>
    ) 
}
}

export default AudioPlayer