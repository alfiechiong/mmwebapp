import styled from 'styled-components'
import Microphone from '../../../assets/Mic.png'

export const Mic = styled.div`
background-image:url(${Microphone})
background-position:center;
background-size:cover;
background-repeat:no-repeat;

`

export const PlayButton = styled.div`
cursor:pointer;
padding:10px;
border-radius:10px;
background:${props => props.Playing ? '#0ca553c2': '#4e7b63'};
font-size:20px;
`


export const FreeSessionlist = styled.div`
border-collapse: collapse;
height:100%;
width:100%;
clear:both;
display:flex;
justify-content:center;
flex-direction:column;



& > ul{
  background:#3a3838e3; 
  width:100%;
  padding:20px;
  display:block;
  color:#fff;
&>li{

  color:white;
  background:url(${Microphone}); 
  background-repeat:no-repeat;
  background-position:center;
  background-size:contain;
  display:block;
  min-width:300px;
  max-width:500px;
  padding:20px;
  margin:20px auto;
  border-radius:10px;
}
}
}
`  

export const AudioViewN = styled.div`
border-collapse: collapse;
height:100%;
width:100%;
clear:both;
display:flex;
justify-content:center;
flex-direction:column;

& > ul{
  background:#3a3838e3; 
  width:100%;
  padding:20px;
  display:block;
  color:#fff;
&>li{

  color:white;
  background:url(${Microphone}); 
  background-repeat:no-repeat;
  background-position:center;
  background-size:contain;
  display:block;
  min-width:300px;
  max-width:500px;
  padding:20px;
  margin:20px auto;
  border-radius:10px;
}
}
}
`  

export const Header = styled.div`  
  height:50px;
  padding:20px;
  text-align:center
  display:flex;
  flex:1;
  align-item:center;
  justify-content:center;

      font-family: Montserrat;
      font-size: 25px;
      font-weight: 600;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: normal;
      text-align: left;
      color: #ffffff; 
  
`