import React from 'react';  
import { Route,Redirect } from 'react-router-dom';
import {Consumer} from '../AppProvider';
import Login from '../Login';
import {db} from '../../firebase'

// Utils
//import auth from '../../utils/auth';

const userType = (email)=>{
    const ref = db.collection('users').where('email','==',email).get();

     ref.then(snap=>{
        //console.log(snap.docs[0].data().type)
        return snap.docs[0].data().type
    })
}



const SubscribersRoute = (props) => (  
    <Consumer>
    {
      ({ state }) => 
      
     // state.email && console.log(state.email)
        
       state.currentUser !== null ? 
       <div>
           {console.log(state.type) }
              <Route exact path={props.path} component={props.component} />
              </div>
      :
     '' 
      
    }
  </Consumer> 
); 

export default SubscribersRoute;