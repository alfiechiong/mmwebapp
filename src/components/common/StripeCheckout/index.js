import React, {Component} from 'react'
import {CardElement, injectStripe,CardExpiryElement,CardNumberElement,PostalCodeElement,CardCVCElement
,PaymentRequestButtonElement} from 'react-stripe-elements'
import {CCard, Pay,Choices} from './Styles'
import {connect} from 'react-redux'
import randomstring from "randomstring"
import {Add,countDocumentWithEventID,onChargedWritten} from './functions'
import {db} from '../../../firebase';
import {Loader} from '../../common/Modal/loader'

const mapStateToProps = state => {
  return { currentUser: state.user.currentUser};
};
class CheckoutForm extends Component {
  constructor(props) {
    super(props);
    this.submit = this.submit.bind(this);
    this.state = {
      complete: false,
      disabled : false,
      show:false
    };

  }

  async xsubmit(ev) {
    // User clicked submit
    // /console.log(ev.target.id)
    let {token} = await this.props.stripe.createToken({name: "Name"});
    let response = await fetch("https://us-central1-mindsetmaestro-c57eb.cloudfunctions.net/charge", {
      method: "POST",
      headers: {"Content-Type": "text/plain"},
      body: token.id
    });
    
    if (response.ok) console.log("successfully charged");
    if (response.ok) this.setState({complete: true});
  }

  async submit(eventId) {
    // User clicked submit

    if (this.state.disabled) {
      return;
  }
  this.setState({disabled: true,show: true});
    
    let {token} = await this.props.stripe.createToken({name: "Name"});

    console.log(token)
    console.log(eventId)
    const data = {
      eventId,
      token,
      email:this.props.currentUser.email,
      docid:this.props.currentUser.userDocId,
      type:this.state.type 
    }

    //if(await countDocumentWithEventID(eventId) > 0){
      Add(data)
    //}

    
    

  }

  onChargedWritten = async (eventId) =>{
    const charged = db.collection('charged').where('idempotency_key','==',eventId);
  
    charged.onSnapshot((querySnapshot)=>{
      querySnapshot.docChanges().forEach(change => {
        if (change.type === 'added') {
          console.log('New charged: ', change.doc.data());
          this.setState({complete: true, show: false});
        }
        if (change.type === 'modified') {
          console.log('Modified charged: ', change.doc.data());
        }
        if (change.type === 'removed') {
          console.log('Removed charged: ', change.doc.data());
        }
      });
    })
  } 

  generateId = () =>{
    
    return (new Date().getTime()+randomstring.generate(7))
  }

  handleRadio = (val)=>{
    this.setState({type:val})
    console.log(val)
  }

  showModal = () => {
    this.setState({ show: true});
  };



  hideModal = () => {
    this.setState({ show: false });

  }

  render() {
    const eventid = this.generateId()
    {this.onChargedWritten(eventid)}
    if (this.state.complete) return <h1>Thank You For Subcribing <br /> Please Wait While System is Generating Questions</h1>;
    return (
      <div className="checkout">
          <Choices >
          <li><input type="radio" name='subscriber' value="monthly" onClick={(e)=>{this.handleRadio(e.target.value)}}/> 
            <span>$19.00 / </span>Billed Monthly</li>
          <li><input type="radio" name='subscriber' value="yearly" onClick={(e)=>{this.handleRadio(e.target.value)}}/> 
            <span>$16.00 / </span>Billed Yearly</li>
        </Choices>  
         <CCard><CardElement /></CCard> 

    {/*     <CCard><label>CardNumber:<CardNumberElement/></label></CCard>
        <CCard><label>Expiration:<CardExpiryElement /></label></CCard>
         <CCard><label>CVC:<CardCVCElement /></label></CCard>
        <CCard><label>Zip:<PostalCodeElement /></label></CCard> */}
    


      
        <Pay disabled={this.state.disabled} id={eventid} onClick={(e)=>this.submit(e.target.id)}>Pay Now</Pay>
        <Loader show={this.state.show} handleClose={this.hideModal}/>
      </div>
    );
  }
}

export default injectStripe(connect(mapStateToProps)(CheckoutForm));