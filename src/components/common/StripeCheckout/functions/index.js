import {db} from '../../../../firebase';
const collectionName = 'Payment';
export const Add = async (data) => {
   
  //if(await countDocumentWithEventID == 0){
    db.collection(collectionName).add({
      eventId: data.eventId,
      token:data.token,
      email:data.email,
      docid:data.docid,
      subcriptionType:data.type,
      date:new Date().getTime()
    }).then((docRef) => {
      console.log("Document written with ID: ", docRef.id);
      })
          .catch((error)=> {
          console.error("Error adding document: ", error);
  });
  }
//};

export const countDocumentWithEventID = async (eventId)=>{
  return await db.collection(collectionName).where('eventId','==',eventId).get().then((snapshot) => {
    console.log(snapshot.docs.length);
      return snapshot.docs.length
    })
        .catch((error)=> {
        console.error("Error adding document: ", error);
});
}

export const onChargedWritten = async (eventId) =>{
  const charged = db.collection('charged').where('idempotency_key','==',eventId);

  charged.onSnapshot((querySnapshot)=>{
    querySnapshot.docChanges().forEach(change => {
      if (change.type === 'added') {
        console.log('New charged: ', change.doc.data());
        //this.setState({complete: true});
      }
      if (change.type === 'modified') {
        console.log('Modified charged: ', change.doc.data());
      }
      if (change.type === 'removed') {
        console.log('Removed charged: ', change.doc.data());
      }
    });
  })
} 