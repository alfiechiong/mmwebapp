import styled, { css } from 'styled-components'

export const CCard = styled.div`
max-width:300px;
border:solid thin #ccc;
padding:10px;
border-radius:10px;
margin:0 auto;
background-color:#fff;
`

export const Pay = styled.button`
max-width:300px;
text-align:center;
border-radius:10px;
font-weight:bold;
font-size:15px;
padding:10px;
margin-top:10px;
background:'aliceblue';
`

export const Choices = styled.ul`

      list-style-type:none;
      margin:20px 0;
      padding:0;
      color:#fff;


      &>li{
        background:#666;
        margin-top:10px;
        padding:20px;
        
        &>span{
          font-size:30px;
          margin:10px;
          
        }
      }

      
    
    
    `