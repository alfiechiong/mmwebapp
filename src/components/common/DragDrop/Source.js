import React, { Component } from 'react'
import {SourceContainer} from './Styles'
import { DragSource } from 'react-dnd';
//import { connect } from 'net';

const itemSource ={

    beginDrag(props, monitor, component) {
        console.log()
        const item = { id: props.id };
        return item;
    },
    endDrag(props, monitor, component){
        if (!monitor.didDrop()) {
            // You can check whether the drop was successful
            // or if the drag ended but nobody handled the drop
            return;
          }
      
          // When dropped on a compatible target, do something.
          // Read the original dragged item from getItem():
          const item = monitor.getItem();
      
          // You may also read the drop result from the drop target
          // that handled the drop, if it returned an object from
          // its drop() method.
          const dropResult = monitor.getDropResult();
          console.table([{item:props.item},{dropResult:dropResult}]);
          props.handleDrop(item.id, props.item)
        }


}

const collect = (connect,monitor) =>{
    return {
        connectDragSource:connect.dragSource(),
        connectDragPreview:connect.dragPreview(),
        isDragging: monitor.isDragging(),
    }
}

class Source extends Component {
    render()
    {
        
    
    const { isDragging, connectDragSource, item } = this.props;
    const opacity = isDragging ? 0 :1 ;
    return connectDragSource(
        <div style={{opacity}}>
        <SourceContainer>
        {item.name}
        {isDragging && ' (and I am being dragged now)'}
        </SourceContainer>
        </div>
    );
    
   /*  render(){
    return(
    <SourceContainer>{this.props.item}</SourceContainer>
    ) 
    } */
}

}

export default DragSource('item',itemSource,collect)(Source);