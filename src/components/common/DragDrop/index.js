import React, { Component } from 'react';
import {DragDropContext} from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import Source from './Source'
import Target from './Target'
import Styles from './Styles';

class DragDrop extends Component
{
    state = {
        items:[
            {id:1,name:'item1'},
            {id:2,name:'item2'},
            {id:3,name:'item3'},
            {id:4,name:'item4'},
            {id:5,name:'item5'}
        ],

        target:[

        ]


        
    }

    deleteItem = (id,item) =>{
        console.log(JSON.stringify(item));


        this.setState(prevState=>{
            let items = prevState.items;
            let target = prevState.target;
            const index = items.findIndex(item=> item.id == id);
            console.log(index);

            items.splice(index,1)
            target.push(item)
            console.log(JSON.stringify(this.state.target));
            return {items, target}
        })
    }
    render() {
    return (

        <div>
        
            {this.state.items.map((item,i)=>(
                <Source key={i} item={item} handleDrop={(id, item)=>{this.deleteItem(id,item)}}/>
            ))}
            {/* <div>
            <Source />
            <Source />
            <Source />
            <Source />
            <Target />
            </div> */}

            <Target className='right' val={this.state.target} />
                
         
            </div>
    );
    }
}

export default DragDropContext(HTML5Backend)(DragDrop)