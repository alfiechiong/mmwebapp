import styled from 'styled-components';

export const TargetContainer = styled.div`

border:solid thin #ccc;
height:150px;
width:100px;
padding:10px
float:right;

&> span {
    border-bottom:solid thin #ccc;
    padding:5px;
    width:150px;
    display:block;
}
`

export const SourceContainer = styled.div`
border:solid thin #ccc;
height:50px;
width:100px;
padding:10px;
float:left;
`


