import React, {Component} from 'react'
import {TargetContainer} from './Styles';
import {DropTarget} from 'react-dnd';
import { extend } from '@firebase/util';

const collect = (connect, monitor) =>{
    return{
    connectDropTarget:connect.dropTarget(),
    hovered:monitor.isOver(),
    item:monitor.getItem()
    }
}

class Target extends Component{

    render(){
        const {connectDropTarget,hovered,item}=this.props;

        const BackgroundColor=hovered?'lightgreen':'white';
        return connectDropTarget(
            <div style={{background:BackgroundColor}}>
            <TargetContainer style={{background:BackgroundColor}}>
            {this.props.val.map((item,i)=>(
                <span>{item.name}</span>
            ))}
            </TargetContainer>
            </div>
        )
    }   

}

export default DropTarget('item', {}, collect)(Target);