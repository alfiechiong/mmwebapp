import React from 'react'
import {PowerUp,UpgradBotton} from './Styles'
import {withRouter} from 'react-router-dom'

export const PowerUpgradeComp = withRouter((props)=>
(
    <PowerUp>
        <div>Personalized Content Selected For You base to the answers of our indepth quiz</div>

        <UpgradBotton onClick={()=>{
            props.history.push('/powerups')
        }}>Power Up Program</UpgradBotton>
    </PowerUp>

))


