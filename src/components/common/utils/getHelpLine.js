import {db} from '../../../firebase'

export const getHelpLine = (country) =>{
    console.log("country",country);
    const helpline = db.collection('TalkPersons');

    return new Promise((res,rej)=>{
        helpline.where("country",">=",country).orderBy("country","asc").limit(1).get().then(
            data =>{
                data.docs.map((item)=>{
                    console.log(item.data())
                })
                res(data)
            }
        ).catch(err=>{
            rej(err)
        })
    })
    
}