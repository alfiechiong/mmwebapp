import React,{Component} from 'react'
import Styles from './Styles'
import {} from './functions'
import {connect} from 'react-redux'
import {setAgreement} from '../../../redux/actions'

import { from } from 'rxjs';

const mapDispatchToProps = dispatch => {
    return {
        setAgreement: () => dispatch(setAgreement()),
   
    };
  };

  const mapStateToProps = state => {
    return { aggreement: state.agreement.aggreement
       
    };
  };

class Agreement extends Component {

    constructor(props) {
        super(props);
        this.state = {
          agreed: false
        };
    
        this.handleInputChange = this.handleInputChange.bind(this);
      }
    
      handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.props.setAgreement();
    
        this.setState({
          [name]: value
        });
      }

    render(){
        return(
            <div> 
            <div>
            There are a few things that you need to agree to, so we are clear what you can expect from this app.
            </div>    
            <label>
            I have read and agree
            <input
              name="agreed"
              type="checkbox"
              checked={this.state.agreed}
              onChange={this.handleInputChange} />
          </label></div>
        )
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Agreement);