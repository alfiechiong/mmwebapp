import React, {
  Component
} from 'react';

import AppProvider from './AppProvider';
import Myrouter from './common/navigation/Myrouter';


class App extends Component {
  render() {
    return (
      <AppProvider test-data='tesing'>
      <Myrouter />
      </AppProvider>
    );
  }
}

export default App;