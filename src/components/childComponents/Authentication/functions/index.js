

const sendEmailVerification = (user, host) =>{

    const actionCodeSettings = {
        url: 'http://'+host+'/?email=' + user.email,
         iOS: {
          bundleId: 'com.example.ios'
        },
        android: {
          packageName: 'com.example.android',
          installApp: true,
          minimumVersion: '12'
        }, 
        handleCodeInApp: true
      };


    !user.emailVerified && user.sendEmailVerification(/* actionCodeSettings */).then(()=> {
                        // Email sent.
                        console.log('Please Verify your email')
                      }).catch(function(error) {
                        // An error happened.
                      })
}

export {sendEmailVerification}