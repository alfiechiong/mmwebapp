import styled, { css } from 'styled-components'


const windowHeight = window.innerHeight + 'px';

export default styled.div`
    height:${windowHeight};
    display:flex;
    
 &>div {
     padding:20px;
    text-align: center;
    margin:auto;
    justify-content:center;
    align-items:center;
  }



  
`
