import React, { Component } from "react"
import firebase from "firebase"
import StyledFirebaseAuth from "react-firebaseui/StyledFirebaseAuth"
import {sendEmailVerification} from './functions'
import { Redirect } from 'react-router-dom'
import Styles from './Styles'
import Agreement from '../../common/aggreement'
import {connect} from 'react-redux'
import {withRouter} from 'react-router-dom'
import bigLogo from '../../../assets/bigLogo.png'

/* firebase.initializeApp({
  apiKey: "AIzaSyDLoqcbTDMFuurtAyDgVEKZ6qwo0j0Osjk",
  authDomain: "fir-auth-tutorial-ed11f.firebaseapp.com"
}) */

const mapStateToProps = state => {
  return { agreement: state.agreement.agreement
  };
};

class Auth extends Component {
  state = { isSignedIn: false }
  uiConfig = {
    signInFlow: "popup",
    
    signInOptions: [
      firebase.auth.GoogleAuthProvider.PROVIDER_ID,
     /*  firebase.auth.FacebookAuthProvider.PROVIDER_ID,
      firebase.auth.TwitterAuthProvider.PROVIDER_ID,
      firebase.auth.GithubAuthProvider.PROVIDER_ID, */
      firebase.auth.EmailAuthProvider.PROVIDER_ID
    ],
    privacyPolicyUrl:()=>{
      alert('asdad');
    },

    callbacks: {

      signInSuccess: () => false //this.props.history.push('Agreement')
      
    }
  }

  

  componentDidMount = () => {
  
    firebase.auth().onAuthStateChanged(user => {
        if(!user.emailVerified || user.emailVerified == null || user.emailVerified === undefined)
           {
                alert('emailSent')
                sendEmailVerification(user,'')
              
              firebase.auth().signOut(); 
              
    } else {
              this.setState({ isSignedIn: !!user })
         }
        
      console.log("user", user)
    })
  }

  render() {
    return (
      <Styles>
      <div className="App">
        {this.state.isSignedIn ? (
          
          <span>
            <div>Signed In!</div>
              <Redirect to='/dashboard' />

            <button onClick={() => firebase.auth().signOut()}>Sign out!</button>
            <h1>Welcome {firebase.auth().currentUser.displayName}</h1>
            <pre>
                {JSON.stringify(firebase.auth())}
            </pre>
            <img
              alt="profile picture"
              src={firebase.auth().currentUser.photoURL}
            />
          </span>

        ) : (
          <div >
          <div>
          <img src={bigLogo}/>
          <StyledFirebaseAuth
            uiConfig={this.uiConfig}
            firebaseAuth={firebase.auth()}
          />
          </div>
    
          <Agreement />
          </div>
        )}
      </div>
      </Styles>
    )
  }
}

export default connect(mapStateToProps,null)(withRouter(Auth))