import React,{Component} from 'react'
import {Qcon,RadioGroup} from './Styles'
import {add,getQuestions} from './functions'
import {connect} from 'react-redux'
import {Loader} from '../../common/Modal/loader'
import {db} from '../../../firebase'
import {withRouter} from 'react-router-dom'

const mapStateToProps = state => {
    return { currentUserId: state.user.currentUser.userDocId
       
    };
  };

class Questionaire extends Component {

    constructor (props, context) {
        super(props, context)

      /* ß */
          
          this.onClickNext = this.onClickNext.bind(this);

          this.state = {
              answers:[],
              questions:[],
              show:false,
                title:Loader,
          }
        }

        componentWillMount = () =>{
            this.setArray();

            
        }

        componentDidMount =async () =>{
            this.setState({questions:await getQuestions()});
        }

        setArray = () =>{
            let array = []
            for(let x=0; x<25; x++){
                array.push('7')
            }

            this.setState({answers:array})
        }
    
        onClickNext() {
            const { steps, currentStep } = this.state;
            this.setState({
              currentStep: currentStep + 1,
            });
          }

      handleChange(e,d) {
        console.log(e)
        console.log(d)
        
        const obj = {};
        obj[d]=e;
        

        this.setState(obj)
        console.log(this.state)
      }
    
      handleRadio = (key,val)=>{
          this.state.answers[key]=val
      }

      submitAnswer = ()=>{
        //console.log(this.state)
       /*  this.state.answers.map((item,k)=>{
            console.log(k,item)
        })  */
        //console.log(this.props.currentUserId)
        add(this.props.currentUserId, this.state.answers)
        this.showModal();
    }


    showModal = (title,data) => {
      
        this.setState({ show: true, title,data});
      };
    
    
    
      hideModal = () => {
        this.setState({ show: false , playing:false });
 
      }

      onCustomCourseWritten = async () =>{
        const cCourse = db.collection('customCourse');
      
        cCourse.onSnapshot((querySnapshot)=>{
          querySnapshot.docChanges().forEach(change => {
            if (change.type === 'added') {
              console.log('New cCOurse: ', change.doc.data());
              console.log(change.doc.id);
                console.log(this.props.id);
              if(change.doc.id === this.props.currentUserId){
                this.setState({show: false});
                this.props.history.push('customcourses')
              }
            }
            if (change.type === 'modified') {
              console.log('Modified cCOurse: ', change.doc.data());
            }
            if (change.type === 'removed') {
              console.log('Removed cCOurse: ', change.doc.data());
            }
          });
        })
      } 

    render(){
/*         const { steps, currentStep } = this.state;
        const buttonStyle = { background: '#E0E0E0', width: 200, padding: 16, textAlign: 'center', margin: '0 auto', marginTop: 32 }; */
        {this.onCustomCourseWritten()}
        return(
            <div>
            <div>Questionaire</div>
            <Qcon>
            {this.state.questions && this.state.questions.map((item,k)=>{
                return(
                    <li key={k}>{item.data.question}
                     {/* <div>
            <Stepper steps={ steps } activeStep={ currentStep }  disabledSteps={ [2] }  />
            <div style={ buttonStyle } onClick={ this.onClickNext }>Next</div>
          </div> */}
                     {/*  <RadioGroup onChange={(e)=> this.handleChange(e,'ans'+(k+1) )} ref={'quz'+k} horizontal id={"q"+k} >
                        <RadioButton value="1" id='aas'>
                            1
                        </RadioButton>
                        <RadioButton value="2">
                            2
                        </RadioButton>
                        <RadioButton value="3">
                            3
                        </RadioButton>
                        <RadioButton value="4">
                            4
                        </RadioButton>
                        <RadioButton value="5">
                            5
                        </RadioButton>
                        <RadioButton value="6">
                            6
                        </RadioButton>
                        <RadioButton value="7"  defaultChecked>
                            7
                        </RadioButton>
                        </RadioGroup>  */}
                        <br />
<RadioGroup>
<label>1<input type="radio" name={k} value="1" onClick={(e)=>{this.handleRadio(e.target.name, e.target.value)}}/> </label>
<label>2<input type="radio" name={k} value="2" onClick={(e)=>{this.handleRadio(e.target.name, e.target.value)}} /></label>
<label>3<input type="radio" name={k} value="3" onClick={(e)=>{this.handleRadio(e.target.name, e.target.value)}} /></label>
<label>4<input type="radio" name={k} value="4" onClick={(e)=>{this.handleRadio(e.target.name, e.target.value)}} /></label>
<label>5<input type="radio" name={k} value="5" onClick={(e)=>{this.handleRadio(e.target.name, e.target.value)}} /></label>
<label>6<input type="radio" name={k} value="6" onClick={(e)=>{this.handleRadio(e.target.name, e.target.value)}} /></label>
<label>7<input type="radio" name={k} value="7" onClick={(e)=>{this.handleRadio(e.target.name, e.target.value)}} defaultChecked /></label>
</RadioGroup>
                    </li>
                   
                )
            })}
            </Qcon>


<button onClick={()=>{this.submitAnswer()}}>Submit</button>
<Loader show={this.state.show} handleClose={this.hideModal}/>
            </div>
        )
    }
}

export default connect(mapStateToProps)(withRouter(Questionaire))