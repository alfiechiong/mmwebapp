import styled from 'styled-components'

export const Qcon = styled.ul`

list-style:none;

li{
font-weight:bold;
font-size:25px;
margin 10px;
padding:10px;
border:solid thin #ccc;
border-radius: 10px 0 10px; 0;
text-align:left;
background-color:#fff;
}

label{
    font-size:15px;
}
`

export const RadioGroup = styled.div`
margin:10px;
padding:10px;

&>label{
    padding:10px;
    margin:10px

}

input{
    margin:10px;
    padding:10px;
}
`