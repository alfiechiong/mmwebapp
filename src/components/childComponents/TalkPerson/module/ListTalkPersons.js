import React from 'react';
import { connect } from "react-redux";
import {ClearFix} from '../../../common/ClearFIx'
import {Header, BottomTable, AddSessionBtn,Pagination,TalkPersonlist, AddTalkPersonBtn} from '../Styles'
import {GetAll} from '../functions'
import {getDbTalkPersons} from '../../../../redux/actions'
import TalkPersonIcon from  '../../../../assets/users.svg'
import {Modal} from '../../../common/Modal/modal';
import AddTalkPerson from './AddTalkPerson';



const mapDispatchToProps = dispatch => {
  return {
    getDbTalkPersons: dbTalkPersons=>dispatch(getDbTalkPersons())
  };
};

const mapStateToProps = state => {
 // return { dbTalkPersons: state.TalkPerson.dbTalkPersons[0]};
};

class ListTalkPersons extends React.Component {

  constructor(props){
    super(props);

    this.state={
      TalkPersons:this.props.dbTalkPersons,
      TalkPerson:[],
      show:false
    }
  }

  componentDidMount =  async() => {
   this.setState({TalkPerson:await GetAll()});
   //console.log('conso ', await GetAll())
   //this.props.getDbTalkPersons(await GetAll())
  }

  getTalkPersonsFromDb = async () =>{
    return await GetAll();
  }

  renderTalkPerson = async () =>{
   //return await this.state.TalkPerson;
  }
  
  addTalkPerson = (key,data)=>{
    this.showModal(key, data)
  }

  showModal = (key,data) => {
    //var me = this.state.ses[key].value = data;
    this.setState({ show: true, });

     //this.setState({me})
  };

  hideModal = async () => {
    
    this.setState({ show: false });
    //this.props.getDbTalkPersons(await GetAll())
    this.setState({TalkPerson:await GetAll()});
  };

 render(){
//  console.log('00',this.state.TalkPerson)
return (
  <div>
    <div>
   
    <Header><img src={TalkPersonIcon} width='40' height='40'/><div>TalkPersonS</div></Header>      
    
    <TalkPersonlist>
      <tbody>
      <tr><th>Name</th><th>Email</th><th>Phone No</th></tr>

  { 
   this.state.TalkPerson && (this.state.TalkPerson.map((itemData, i) =>(
    <tr key={itemData.id}>
    <td> {itemData.data.fullname}</td>
    <td> {itemData.data.email}</td>
    <td> {itemData.data.phone}</td>
    </tr>
  )))  }
  </tbody>
  </TalkPersonlist>

  <BottomTable>
  <Pagination> </Pagination>
  <AddTalkPersonBtn onClick={()=>{this.addTalkPerson()}}>Add New Talk Person</AddTalkPersonBtn>
  </BottomTable>
  </div>

   <Modal show={this.state.show} handleClose={this.hideModal} title='Add New Talk Person' >
            {
              <AddTalkPerson />
            }
        </Modal>
  </div>
    );
  }
}

const ListTalkPerson = connect(mapStateToProps, mapDispatchToProps)(ListTalkPersons)
export default ListTalkPerson;
