import {db} from '../../../../firebase';

  const collectionName = 'TalkPersons';

  const Add = (fullname, phone, email) => {
    if(fullname !== null && fullname !== '' && email !== null && email !== '')
    {
      db.collection(collectionName).add({
        fullname: fullname,
        email: email,
        phone:phone
      }).then((docRef) => {
        console.log("Document written with ID: ", docRef.id);

        window.alert('Added New Data');
        })
            .catch((error)=> {
            console.error("Error adding document: ", error);
    });
    }else{
        alert( 'all fields cannot be empty');
    }
  };

  const GetAll = async ()=>{
    var docRef = db.collection(collectionName);

    return await new Promise((res, rej)=>{
    docRef.onSnapshot((querySnapshot)=> {
      const mydata = querySnapshot.docs.map((data)=>({
        id:data.id,
        data:data.data()
      }))
      console.log('data', mydata)
      res(mydata);


    })
  })
  }

  const GetAllWithLimit = ()=>{
    var docRef = db.collection(collectionName).where("name","==","alfiechiong" ).get();

    return new Promise((res, rej)=>{
    docRef.then((querySnapshot)=> {
      const mydata = querySnapshot.docs.map((data)=>({
        id:data.id,
        data:data.data()
      }))
      console.log(mydata)
      res(mydata);
  
    }).catch(function(error) {
      rej(error)
        console.log("Error getting document:", error);
    });
  })
  }



  export {Add, GetAll,GetAllWithLimit}