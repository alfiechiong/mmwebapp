import React from 'react';
import {Add} from './functions';
import {Form, Field} from 'react-final-form'
import {Required, Email, Phone} from '../../validator'


class TalkPerson extends React.Component {
    constructor() {
        super();
        this.state = {
         email: '',
         fullname: ''
        };
      }

      updateInput = e => {
        this.setState({
          [e.target.name]: e.target.value
        });

      }

      addUser = e => {
        Add(e.fullname, e.phone, e.email)
        
      };

  //required = value=>value ? undefined:'required';
  render() {
    return (
      <Form onSubmit={this.addUser} >
    {({ handleSubmit, values, submitting })=>(
      <form onSubmit={handleSubmit}>
      <Field 
      name='fullname' 
      placeholder='fullname' 
      validate={Required}
      >
      {({input, meta, placeholder}) =>(
       <div>
      <label>Name</label>
      <input {...input} placeholder={placeholder} />
      {meta.error && meta.touched && <span>{meta.error}</span>}
      </div>
      )}
      </Field>

      <Field 
      name='email' 
      placeholder='email' 
      validate={Email}
      >
      {({input, meta, placeholder}) =>(
       <div>
      <label>Email</label>
      <input {...input} placeholder={placeholder} type='email'/>
      {meta.error && meta.touched && <span>{meta.error}</span>}
      </div>
      )}
      </Field>

      <Field 
      name='phone' 
      placeholder='phone' 
      validate={Phone}
      >
      {({input, meta, placeholder}) =>(
       <div>
      <label>Phone</label>
      <input {...input} placeholder={placeholder} type='number'/>
      {meta.error && meta.touched && <span>{meta.error}</span>}
      </div>
      )}
      </Field>

      <button type='submit' disbaled={submitting}>Submit</button>

      </form>
    )}
    </Form>
        );
      }
   }
export default TalkPerson;