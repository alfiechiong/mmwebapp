import React from 'react';
import {Form, Field} from 'react-final-form'


  const sleep = ms =>new Promise(resolve =>setTimeout(resolve, ms))

  const showResult = async values=>{
    await sleep(500)
    window.alert(JSON.stringify(values, undefined, 2))
  }

  const required = value=>value ? undefined:'required';
  const Member = () => (
    <Form onSubmit={showResult} >
    {({ handleSubmit, values, submitting })=>(
      <form onSubmit={handleSubmit}>
       
          <Field 
          name='name' 
          placeholder='Name' 
          validate={required}
          >
          {({input, meta, placeholder}) =>(
           <div>
          <label>Name</label>
          <input {...input} placeholder={placeholder} />
          {meta.error && meta.touched && <span>{meta.error}</span>}
          </div>
          )}
          </Field>
       

       <Field 
          name='lname' 
          placeholder='LName' 
          validate={required}
          >
          {({input, meta, placeholder}) =>(
           <div>
          <label>LName</label>
          <input {...input} placeholder={placeholder} />
          {meta.error && meta.touched && <span>{meta.error}</span>}
          </div>
          )}
          </Field>

          <Field 
          name='email' 
          placeholder='Email' 
          validate={required}
          >
          {({input, meta, placeholder}) =>(
           <div>
          <label>Email</label>
          <input {...input} placeholder={placeholder} />
          {meta.error && meta.touched && <span>{meta.error}</span>}
          </div>
          )}
          </Field>

         <div>
          <label>FieldStateTest</label>
          <Field name='name' placeholder='fieldstate' 
          validate={required}
          >
          {fieldstate=>(
            <pre>
              {JSON.stringify(fieldstate, undefined, 2)}
            </pre>
          )}
          </Field>
        </div>
       <button type='submit' disbaled={submitting}>Submit</button>
       <pre>
         {
           JSON.stringify(values, undefined, 2)
         }
         </pre>
      </form>
    )}
    </Form>
       /*  ({values, handleChange}) =>
        (
          <form>
          <input type="email"
          name="email"
          placeholder="Full name"
          onChange={handleChange}
          value={values.email}
        />
        <input type="password"
          name="password"
          placeholder="Password"
          onChange={handleChange}
          value={values.email}
        />
        </form>
        ) */
  )


export {Member};