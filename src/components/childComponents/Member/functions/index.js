import {db} from '../../../../firebase';

const Add = (fullname, email) => {
    if(fullname !== null && fullname !== '' && email !== null && email !== '')
    {
      db.collection('Users').add({
        fullname: fullname,
        email: email
      }).then((docRef) => {
        console.log("Document written with ID: ", docRef.id);
        })
            .catch((error)=> {
            console.error("Error adding document: ", error);
    });
    }else{
        alert( 'all fields cannot be empty');
    }
  };


  export {Add}