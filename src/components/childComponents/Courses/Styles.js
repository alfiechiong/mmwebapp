import styled, { css } from 'styled-components'
import {media} from '../../common/media'

export const CourseSessionList = styled.ul`
padding:0;
margin:0;

li{
  text-align:left;
  padding:20px;
  font-size:12px;
  border-bottom:solid thin #ccc;
  border-radius:0;
  

  display:block;

  background:#fbfbfbbf;

  margin:10px auto;
 


  
}

`

export const Header = styled.div`

${media.desktop` 
margin:0 auto;
max-width:500px;`
}
  
${media.tablet` margin:0 auto;
  max-width:400px;`
}
  
${media.phone` margin:0 auto;
  max-width:100%;`
}
 


  &>div{
      padding:20px;    
      height: 17px;
      font-family: Montserrat;
      font-size: 24px;
      font-weight: 600;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: normal;
      text-align: center;
      color: #333;
  }

 
`

export const CourselistSubscribers = styled.div`
&>ul{
  display:flex;
  height:100%;
  justify-content:center;
  cursor:pointer;
  

  &>li{
    display:block;
    width:400px;
    background:#fbfbfbbf;
    padding:20px;
    margin:10px auto;
    color:#333;

   
      
    ${media.tablet` 
      margin:0 auto;
      max-width:400px;`
    }
      
    ${media.phone` 
      margin:0 auto;
      max-width:200px;`
    }
  }

  div{
    font-size:20px;
    font-weight:bold;
    padding:10px;
    span{
        font-size:15px;
        color:#501a2b;

        @media (max-width: 400px) {
          font-size:10px;
        }
    }
  }
}
    `
export const Courselist = styled.div`
&>ul{
  display:flex;
  height:100%;
  justify-content:center;
  

  li{
    display:block;
    min-width:400px;
    max-width:500px;
    border-radius:10px;
  
    background:#ccc;
    padding:20px;
    margin:10px auto;
    color:rgba(80, 76, 76, 0.6);

    ${media.tablet` 
      
      max-width:200px;`
    }
      
    ${media.phone` 
       max-width:200px;`
    }
  }

  div{
    font-size:20px;
    font-weight:bold;
    padding:10px;
    span{
        font-size:15px;
        color:#fff;
        @media (max-width: 400px) {
          font-size:10px;
        }
    }
  }
}
    `

    export const BottomTable = styled.div`
    margin-top:20px;
    padding:10px;
   

    &>div{
      float:left;
      width:47%;

    }
  `

  export const EditCourses = styled.div`
  display:block;
  padding:2px;
  cursor:pointer;
  width:100px;
  color:white;
  border-radius:5px;
  text-align:center;
  background:#00a89c
  `  

export const CourseBtn = styled.div`
display:block;
height:20px;
border:solid thin #ccc;
padding:10px;
margin:5px;
cursor:pointer;
`  

export const AddSesBtn = styled.div`
height: 40px;
box-shadow: 2.8px 2.8px 0 0 rgba(207, 207, 209, 0.75);
background-color:#2e4f80;
border-radius:10px;
text-align:center;
padding-left:150px;
font-size: 20px;
font-weight: 600;
font-style: normal;
color:#ffffff;
cursor:pointer;
`  

export const SessionListContainer = styled.div`
border:solid thin #ccc;
display:block;
padding:15px;

& > div {
  display:block;
  & > div {
    font-size:20px;
    font-weight:bold;
    border:solid thin #ccc;
    border-radius:5px;
    text-align:center;
    padding:0px 10px;
    float:right;
    cursor: pointer;

  }
}
`

export const Pagination = styled.div`
height: 40px;
padding:10px;
`  
export const AddCourseBtn = styled.div`
width: 346px;
height: 40px;
padding-top:10px;
box-shadow: 2.8px 2.8px 0 0 rgba(207, 207, 209, 0.75);
background-color:#2e4f80;
border-radius:10px;


  > a {
    text-align:center;
    font-family: Montserrat;
    font-size: 20px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #ffffff;
    text-decoration:none;
  }
`
export const SubmitBtn = styled.button`
padding:10px;
font-size:12px;
font-weight:bold;
background:#c4dde8;
border-radius:5px;
`    

export const ListCourseSes = styled.li`
padding:10px;
font-size:12px;
font-weight:bold;
border-radius:5px;
border:solid thin #333;
margin-top:10px;

span{
  padding:10px;
}

label{
  padding:10px;
  font-size:20px;

}
`    
export const SearchFilter = styled.div`
border:solid thin #333;
padding:10px;
font-weight:bold;

input{
  height:25px;
  margin-left:10px;
  width:50%;
}

`

const btn = (light, dark) => css`
  white-space: nowrap;
  display: inline-block;
  border-radius: 5px;
  padding: 5px 15px;
  font-size: 16px;
  color: white;
  &:visited {
    color: white;
  }
  background-image: linear-gradient(${light}, ${dark});
  border: 1px solid ${dark};
  &:hover {
    background-image: linear-gradient(${light}, ${dark});
    &[disabled] {
      background-image: linear-gradient(${light}, ${dark});
    }
  }
  &:visited {
    color: black;
  }
  &[disabled] {
    opacity: 0.6;
    cursor: not-allowed;
  }
`

const btnDefault = css`
  ${btn('#ffffff', '#d5d5d5')} color: #555;
`

const btnPrimary = btn('#4f93ce', '#285f8f')
const btnDanger = btn('#e27c79', '#c9302c')

export default styled.div`
  font-family: sans-serif;

  h1 {
    text-align: center;
    color: #222;
  }

  h2 {
    text-align: center;
    color: #222;
  }

  & > div {
    text-align: center;
  }

  a {
    display: block;
    text-align: center;
    color: #222;
    margin-bottom: 10px;
  }

  p {
    max-width: 500px;
    margin: 10px auto;
    & > a {
      display: inline;
    }
  }

  form {
    max-width: 800px;
    margin: 10px auto;
    border: 1px solid #ccc;
    padding: 20px;
    box-shadow: 2px 2px 5px rgba(0, 0, 0, 0.3);
    border-radius: 3px;
    position: relative;
    text-align:left;

    label{
      font-size:15;
      font-weight:bold;
      margin: 20px 0px;
    }
    input, textarea {
        flex: 1;
        padding: 5px 10px;
        font-size: 1em;
        margin-bottom:20px;
        border: 1px solid #ccc;
        border-radius: 3px;
        width:100%;
      }


    .loading {
      text-align: center;
      display: block;
      position: absolute;
      background: url('https://media.giphy.com/media/130AxGoOaR6t0I/giphy.gif')
        center center;
      background-size: fill;
      font-size: 2em;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      padding: 50px 0 0 0;
      z-index: 2;
    }

    & > div {
      display: flex;
      flex-flow: row nowrap;
      line-height: 2em;
      margin: 5px;
      position: relative;
      & > label {
        color: #333;
        width: 110px;
        min-width: 60px;
        font-size: 1em;
        line-height: 32px;
      }
      & > input,
      & > select,
      & > textarea {
        flex: 1;
        padding: 3px 5px;
        font-size: 1em;
        margin-left: 15px;
        border: 1px solid #ccc;
        border-radius: 3px;
      }
      & > input[type='checkbox'] {
        margin-top: 7px;
      }
      & > div {
        margin-left: 16px;
        & > label {
          display: block;
          & > input {
            margin-right: 3px;
          }
        }
      }
      & > span {
        line-height: 32px;
        margin-left: 10px;
        color: #800;
        font-weight: bold;
      }
      & > button.remove {
        ${btnDanger};
      }
    }
    & > .buttons {
      display: flex;
      flex-flow: row nowrap;
      justify-content: center;
      margin-top: 15px;
    }

    .error {
      display: flex;
      font-weight: bold;
      color: #800;
      flex-flow: row nowrap;
      justify-content: center;
    }
    pre {
      position: relative;
      border: 1px solid #ccc;
      background: rgba(0, 0, 0, 0.1);
      box-shadow: inset 1px 1px 3px rgba(0, 0, 0, 0.2);
      padding: 20px;
    }
  }
  button {
    margin: 0 10px;
    &[type='submit'] {
      ${btnPrimary};
    }
    &[type='button'] {
      ${btnDefault};
    }
  }
`
