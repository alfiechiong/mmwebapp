import React, { Component } from 'react';
import {Add} from '../functions'
import {withRouter} from 'react-router-dom';
import {Form, Field} from 'react-final-form'
import {Required} from '../../../validator'
import {connect} from 'react-redux'
import Styles, {CourseBtn,SessionListContainer,AddSesBtn,SearchFilter} from '../Styles'
import { getDbSessions,getDbCourses } from '../../../../redux/actions';
import {Modal} from '../../../common/Modal/modal'
import {getAllSessions} from '../../Session/functions'
import {getAllCourses} from '../../Courses/functions'

import '../../../common/Modal/style.css';

const mapDispatchToProps = dispatch => {
  return {
    getDbSessions: dbSessions=>dispatch(getDbSessions()),
    getDbCourses: dbCourses=>dispatch(getDbCourses())
  };
};

const mapStateToProps = state => {
  return { 
    dbSessions: state.session.dbSessions[0]

  };
};

class AddCourses extends Component {

  constructor(props){
    super(props);

    props.getDbSessions(this.getSessionsFromDb())

    this.state={
    sessions:this.props.dbSessions,
     
    ses:[],
    show:false,
    selected:null,
    sessionFilter:''
    }
  }

  getSessionsFromDb = async () =>{
    return await getAllSessions()
  }
 
  showModal = (key,data) => {
    var me = this.state.ses[key].value = data;
    this.setState({ show: true, selected:key, me });

     //this.setState({me})
  };

  hideModal = () => {
    this.setState({ show: false });
  };

  selectedSession = (key,val,link,description)=>{
   // window.alert(key, val)

    var me = this.state.ses[key].value = val;
    var me = this.state.ses[key].link = link;
    var me = this.state.ses[key].description = description;
  /*   var me = this.setState(
      {
        ses:this.state.ses[key].value=val, 
        ses:this.state.ses[key].link=link 
      })   */
    this.setState({ show: false, me});
    console.log(this.state.ses)
  }

 addCourse = e => {

    
    const data={
        name:e.name,
        description:e.description,
        session:this.state.ses
        //session:e.selectedSession
    }
    Add(data);
    this.props.getDbCourses(getAllCourses())
    this.props.history.push('/content');
  };

  getses(key,data){

   this.showModal(key, data)
   // var me = this.state.ses[key].value = 'red';

   // this.setState({me})
    //window.alert(JSON.stringify(this.state.ses))
//return 'the ses';
  }

  removeSes(key,data){

    const newsess = this.state.ses.splice(key, 1);

    this.setState({newsess});

   // var me = this.state.ses[key].value = val;
   // this.setState({ show: false, me});
  }

  newSes = ()=>{
      
    this.setState({ses:
      [...this.state.ses,{name:('sess'+this.state.ses.length), value:('values'+this.state.ses.length)}]})
  }

  updateInput = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  }


    
    newSession = ()=>{
      
      this.setState({sessions:
        [...this.state.sessions,{name:('session'+this.state.sessions.length)}]
      })
    }

    filterRes = (item) =>{

        const name = item.data.name.toLowerCase();

        return name.indexOf(this.state.sessionFilter) > -1;            
    }

  render() {
    return (
        <Styles>
         <Form onSubmit={this.addCourse} >
    {({ handleSubmit, values, submitting })=>(
      <form onSubmit={handleSubmit}>
      <Field 
      name='name' 
      placeholder='name' 
      validate={Required}
      >
      {({input, meta, placeholder}) =>(
       <div>
      <label>Name</label>
      <input {...input} placeholder={placeholder} />
      {meta.error && meta.touched && <span>{meta.error}</span>}
      </div>
      )}
      </Field>

      <Field 
      name='description' 
      placeholder='description' 
      validate={Required}
      >
      {({input, meta, placeholder}) =>(
       <div>
      <label>Description</label>
      <textarea {...input} placeholder={placeholder} type='text' maxLength='1000'/>
      {meta.error && meta.touched && <span>{meta.error}</span>}
      </div>
      )}
      </Field>

      <h2>Course Sessions</h2>  

        {this.state.ses && this.state.ses.map((data,key)=>{
         
          return(
            <SessionListContainer key={key}>
            <span>{data.value}</span>
            <div onClick={()=>{this.getses(key,'data'+key)}}><div>+</div></div>
            <div onClick={()=>{this.removeSes(key,'data'+key)}}><div>x</div></div>
           
            </SessionListContainer>
         )
        })} 

      <AddSesBtn onClick={()=>{this.newSes()}}>Add Session to course</AddSesBtn>

      <button type='submit' disabled={submitting}>Submit</button>

      </form>
    )}
    </Form>

    <Modal show={this.state.show} handleClose={this.hideModal} title='Sessions ' >

    <SearchFilter>Search:<input onChange={this.updateInput} name='sessionFilter'   /></SearchFilter>
          {
             this.props.dbSessions && (this.props.dbSessions.filter(session => this.filterRes(session, this.state.sessionFilter)).map((itemData, i) =>(
          
              <CourseBtn key={i} onClick={()=>{
                console.log(itemData.data.description);
                this.selectedSession(this.state.selected,itemData.data.name,itemData.data.link,itemData.data.description)}}>{itemData.data.name} </CourseBtn>
             )))
            }
          <p>Data</p>
        </Modal>
        </Styles>
    )
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AddCourses))
