import React from 'react';
import { connect } from "react-redux";
import {Link, withRouter} from 'react-router-dom';
import {Audioplayer} from '../../../common/audioplayer';
import {GetAll, Add,updateCourse,getAllCourses} from '../functions'
import {addCourses,getDbCourses} from '../../../../redux/actions'
import {Header, Courselist,BottomTable, Pagination, AddCourseBtn,EditCourses,ListCourseSes,CourselistSubscribers} from '../Styles'
import {ClearFix} from '../../../common/ClearFIx';
import {Modal} from '../../../common/Modal/modal';
import Styles,{SubmitBtn} from '../Styles'
const mapDispatchToProps = dispatch => {
  return {
    addCourses: courses => dispatch(addCourses(courses)),
    getDbCourses: dbCourses=>dispatch(getDbCourses(['']))
  };
};

const mapStateToProps = state => {
  return { dbCourses: state.course.dbCourses
  };
};

class ListPaidCourses extends React.Component {

  constructor(props){
    super(props);

    props.getDbCourses(GetAll())

    this.state={
      coursess:props.dbCourses,
      show:false
    }
  }

  componentDidMount = async() =>{
    this.setState({coursess:await this.props.dbCourses})
  }


  showModal = (key,data) => {
    this.setState({ show: true});
    this.setData(key)
  };



  hideModal = () => {
    this.setState({ show: false });
    window.location.reload();
  };

  handleSubmit = (e) =>{

    const data = {
      id:this.state.selectedId,
      name:this.refs.name.value,
      sessions:this.state.selectedSessions,
      description:this.refs.description.value
    }
    console.log(data);
    this.updateCourse(data)
  } 

  setData = (key) =>{
    const name = this.props.dbCourses[key].data.name
    const desc = this.props.dbCourses[key].data.description
    const sessions = this.props.dbCourses[key].data.sessions
    const id = this.props.dbCourses[key].id

    this.refs.description.value = desc;
    this.refs.name.value = name;
    this.refs.sessions.value = sessions;
    this.setState({selectedId: id, selectedSessions:sessions})

   // 
  }

  updateCourse = (data) =>{

    updateCourse(data);
  }

  renderCourses = async () =>{
 
  return   this.state.coursess.map((itemData, i) =>(
    
        <tr key={itemData.id}>
            <td> {itemData.data.name}</td>
            <td><EditCourses onClick={()=>this.showModal(i)}>EDIT</EditCourses></td>
         </tr>   
      ))
     

  }


render() {

return (
  <div>
   
    <Header><div>Available Courses</div></Header>         

 { 
     // this.props.dbCourses && this.props.dbCourses.filter(itemData=>itemData.data.name == 'Decisiveness Course').map((itemData, i) =>(
    this.props.dbCourses && this.props.dbCourses.map((itemData, i) =>(
        <CourselistSubscribers 
        onClick={()=>{
        this.props.history.push({pathname: '/coursesessions',
        search: '?the=search',
        data:itemData.data.sessions
        })}}  
        key={itemData.id}>
        <ul>
          <li>
              <div>{itemData.data.name} <br />
              <span>{itemData.data.description}</span>
          </div></li> 
      </ul>
  </CourselistSubscribers>
  ))}

  <ClearFix />

    <Modal show={this.state.show} handleClose={this.hideModal} title='Update Course ' >
          {
           <div>
           
            <Styles>
            <form>
              
                <label> Name</label>
                 <input type='text' name='name' ref='name'/>

                  <label> Desc</label>
                   <textarea name='description' ref='description'  />

                   <label> Sessions</label>
                 <input type='text' name='sessions' ref='sessions' disabled hidden={true}/>
                <ol>
                 {
                   this.state.selectedSessions && this.state.selectedSessions.map((session,k)=>{
                    console.log('session-',session)
                   return(
                    <ListCourseSes>
                      <label>{session.value}</label>
                      <Audioplayer track={session.link} />
                      <span>Discription: {session.description}</span>
                    </ListCourseSes>
                   )
                   })
                 }

                 
                 </ol>
              
             </form>
             <SubmitBtn onClick={()=>{this.handleSubmit()}}>submit</SubmitBtn>
            </Styles>
            </div>
            }
       
        </Modal>
    </div>

    );
  }
}

const Course = connect(mapStateToProps, mapDispatchToProps)(withRouter(ListPaidCourses))
export default Course;