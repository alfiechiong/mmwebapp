import React from 'react';
import { connect } from "react-redux";
import {Link, withRouter} from 'react-router-dom';
import AudioView from '../../../common/audio/AudioViewNoUp';
import {GetMyCustomCourses, Add,updateCourse,getAllCourses} from '../functions'
import {addCourses,getDbCustomCourses} from '../../../../redux/actions'
import {Header, Courselist,BottomTable, Pagination, AddCourseBtn,EditCourses,ListCourseSes,CourselistSubscribers} from '../Styles'
import {ClearFix} from '../../../common/ClearFIx';
import {Modal} from '../../../common/Modal/modal';
import Styles,{SubmitBtn, CourseSessionList} from '../Styles'
import {withConsumer} from '../../../common/withContext'
const mapDispatchToProps = dispatch => {
  return {
    addCourses: courses => dispatch(addCourses(courses)),
    getDbCustomCourses: docid=>dispatch(getDbCustomCourses(docid))
  };
};

const mapStateToProps = state => {
  return { 
    dbCustomCourses: state.course.dbCustomCourses,
    userId:state.user.currentUser.userDocId
  };
};

class ListCustomCourses extends React.Component {

  constructor(props){
    super(props);


    this.state={
      coursess:props.dbCustomCourses,
      show:false,
      title:undefined,
      data:undefined,
      playing:false
    }
  }

  componentDidMount = async() =>{
    this.props.getDbCustomCourses(this.props.userId)

    this.setState({coursess:await this.props.dbCustomCourses})

    console.log("conse", this.props.state)
  }


  showModal = (title,data) => {
    this.props.state.setAudioPlay();
    this.setState({ show: true, title,data});

    //this.setData(key)
  };



  hideModal = () => {
    this.setState({ show: false , playing:false });
   // window.location.reload();
  };

  handleSubmit = (e) =>{

    const data = {
      id:this.state.selectedId,
      name:this.refs.name.value,
      sessions:this.state.selectedSessions,
      description:this.refs.description.value
    }
    console.log(data);
    this.updateCourse(data)
  } 

  setData = (key) =>{
    const name = this.props.dbCourses[key].data.name
    const desc = this.props.dbCourses[key].data.description
    const sessions = this.props.dbCourses[key].data.sessions
    const id = this.props.dbCourses[key].id

    this.refs.description.value = desc;
    this.refs.name.value = name;
    this.refs.sessions.value = sessions;
    this.setState({selectedId: id, selectedSessions:sessions})

  }

  updateCourse = (data) =>{

    updateCourse(data);
  }

  renderCourses = async () =>{
 
  return   this.state.coursess.map((itemData, i) =>(
    
        <tr key={itemData.id}>
            <td> {itemData.data.name}</td>
            <td><EditCourses onClick={()=>this.showModal(i)}>EDIT</EditCourses></td>
         </tr>   
      ))
     

  }


render() {

return (
  <div>
    <Header><div> Personalized Course</div></Header>     
 { this.props.dbCustomCourses.data && this.props.dbCustomCourses.data.course.map((itemData, i) =>(
      <CourselistSubscribers 
      /* onClick={()=>{
      this.props.history.push({pathname: '/customcoursesessions',
      search: '',
      data:itemData.sessioncourse
      })}}   */
      key={i}>
      <ul>
        <li>
           
            <CourseSessionList>
             {itemData.sessioncourse.map((item,k)=>{
                return(
                   <li key={k} onClick={()=>this.showModal(item.ses.data.name,item.ses.data)}>{item.ses.data.name}</li>
                )
              })} 
              
            </CourseSessionList> 
        </li> 
    </ul>
</CourselistSubscribers>
   )) 
/*      // this.props.dbCourses && this.props.dbCourses.filter(itemData=>itemData.data.name == 'Decisiveness Course').map((itemData, i) =>(
    this.props.dbCourses && this.props.dbCourses.map((itemData, i) =>(
        <CourselistSubscribers 
        onClick={()=>{
        this.props.history.push({pathname: '/coursesessions',
        search: '?the=search',
        data:itemData.data.sessions
        })}}  
        key={itemData.id}>
        <ul>
          <li>
              <div>
               {itemData.data.name} <br />
              <span>{itemData.data.description}</span> 
          </div></li> 
      </ul>
  </CourselistSubscribers>
  
 )) */}

  {/* <ClearFix />
*/
    <Modal show={this.state.show} handleClose={this.hideModal} title={this.state.title} >
          {
         
              <AudioView data={this.state.data} />
            }
       
        </Modal> }
    </div> 
    );
  }
}

const Course = withConsumer(connect(mapStateToProps, mapDispatchToProps)(withRouter(ListCustomCourses)))
export default Course;