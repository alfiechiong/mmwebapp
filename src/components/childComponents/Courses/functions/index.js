import React from 'react'
import {db} from '../../../../firebase';

  const collectionName = 'Courses';
  const Add = (data) => {
   
      db.collection(collectionName).add({
        name: data.name,
        description:data.description,
        sessions:data.session
      }).then((docRef) => {
        console.log("Document written with ID: ", docRef.id);
        })
            .catch((error)=> {
            console.error("Error adding document: ", error);
    });
  };

  const updateCourse= (data) =>{
    console.log(data.id)
    db.collection(collectionName).doc(data.id).set({
      name: data.name,
      sessions: data.sessions,
      description:data.description
    }).then(()=>{
      alert('success')
    })
  }


  const GetAll = ()=>{
    var docRef = db.collection(collectionName);

    return new Promise((res, rej)=>{
    docRef.get().then((querySnapshot)=> {
      const mydata = querySnapshot.docs.map((data)=>({
        id:data.id,
        data:data.data()
      }))

      //console.log(mydata);
      res(mydata);
  
    }).catch(function(error) {
      rej(error)
        console.log("Error getting document:", error);
    });
  })
  }

  const GetMyCustomCourses = (id)=>{
    var docRef = db.collection('customCourse').doc(id);

    return new Promise((res, rej)=>{
    docRef.get().then((querySnapshot)=> {
      const mydata = {
        id:querySnapshot.id,
        data:querySnapshot.data()
      }

      //console.log(mydata);
      res(mydata);
  
    }).catch(function(error) {
      rej(error)
        console.log("Error getting document:", error);
    });
  })
  }

  const getAllCourses = ()=>{
    var docRef = db.collection(collectionName);

    return new Promise((res, rej)=>{
    docRef.get().then((querySnapshot)=> {
      const mydata = querySnapshot.docs.map((data)=>({
        id:data.id,
        data:data.data()
      }))

      //console.log(mydata);
      res(mydata);
  
    }).catch(function(error) {
      rej(error)
        console.log("Error getting document:", error);
    });
  })
  }

  const GetAllSnap = ()=>{
    var docRef = db.collection(collectionName);

    return new Promise((res, rej)=>{
    docRef.onSnapshot((querySnapshot)=> {
       const mydata = querySnapshot.docs.map((data)=>({
        id:data.id,
        data:data.data()
      })) 
      /* //console.log(mydata);
      const mydata = querySnapshot.docs.map((data,i)=>(
        <div key={i}>{data.data.name}</div>
      ))  */
      res(mydata);
  
    })
  })
  }

  const GetCourses = async()=>{
    var docRef = db.collection(collectionName);

    //return new Promise((res, rej)=>{
    const mycourse =  await docRef.onSnapshot((querySnapshot)=> {
       //console.log(querySnapshot.docs[0])
      const mydata = querySnapshot.docs.map((item,i)=>(
        {
          id:item.id,
          name:item.data().name,
          description:item.data().description
        }
        //console.log(item.data())        
      ))   
     // var mydata = (<div>lets get loud</div>)
     console.log('data::',mydata);
     return mydata;
      //return mydata  
    })
    
    console.log('labas: ',mycourse)
    return mycourse;
    //console.log(courses)
    //res(courses) 
    
  //})
  }

  const watchCourses = () =>{
  
  db.collection("Courses")
    .onSnapshot((snapshot) =>{
        //...console
        const doc = snapshot.docChanges().map(change => ({
          
           id: change.doc.id,
          name: change.doc.data().name
        }));

        console.log(doc)
         

    }, (error)=> {
        //...
        console.log(error)
    });
  }
  
  export {Add, GetAll,GetAllSnap,GetCourses,watchCourses,updateCourse,getAllCourses,GetMyCustomCourses}