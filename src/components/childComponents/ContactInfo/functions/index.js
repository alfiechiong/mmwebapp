import {db} from '../../../../firebase';
import { connect } from "react-redux";
import {getDbContact} from '../../../../redux/actions'
import store from '../../../../redux/store'




  const collectionName = 'Contact';

  const mapDispatchToProps = dispatch => {
    return {
        getDbContact: dbContact=>dispatch(getDbContact())
    };
  };

  const Add = (data) => {
   
      db.collection(collectionName).set({
        name: data.name,
        email:data.email,
        phone:data.phone,
        address:data.address
      }).then((docRef) => {
        window.alert('Added New Data');
        console.log("Document written with ID: ", docRef.id);
        })
            .catch((error)=> {
            console.error("Error adding document: ", error);
    });
  };

  const Update = (data) => {
   

    db.collection(collectionName).doc('JDCn8UX8FjJzeTPRdLoT').set({
      name: data.name,
      email:data.email,
      phone:data.phone,
      address:data.address
    }).then((docRef) => {
      console.log("Document written with ID: ", docRef.id);
      store.dispatch(GetAll());
      })
          .catch((error)=> {
          console.error("Error adding document: ", error);
  });
};

  const GetAll = () =>{

    var docRef = db.collection(collectionName).doc('JDCn8UX8FjJzeTPRdLoT');

    return new Promise((res, rej)=>{

      docRef.get().then((doc)=> {
        /* const mydata = doc.data().map((data)=>({
        name: data.name,
        email:data.email,
        phone:data.phone,
        address:data.address
        })) */

        const mydata = {
          name: doc.data().name,
          email:doc.data().email,
          phone:doc.data().phone,
          address:doc.data().address
        }
        console.log(mydata);
        res(mydata);
    }).catch(function(error) {

      rej(error)
      console.log("Error getting document:", error);
    });
  })
}

/* docRef.get().then(function(doc) {
    if (doc.exists) {
        console.log("Document data:", doc.data());
    } else {
        // doc.data() will be undefined in this case
        console.log("No such document!");
    }
}).catch(function(error) {
    console.log("Error getting document:", error);
});
  } */

 /*  const GetAll = ()=>{
    var docRef = db.collection(collectionName);

    return new Promise((res, rej)=>{
    docRef.get().then((querySnapshot)=> {
      const mydata = querySnapshot.docs.map((data)=>({
        id:data.id,
        data:data.data()
      }))

      //console.log(mydata);
      res(mydata);
  
    }).catch(function(error) {
      rej(error)
        console.log("Error getting document:", error);
    });
  })
  } */

  const GetAllSnap = ()=>{
    var docRef = db.collection(collectionName);

    return new Promise((res, rej)=>{
    docRef.onSnapshot((querySnapshot)=> {
       const mydata = querySnapshot.docs.map((data)=>({
        id:data.id,
        data:data.data()
      })) 
      /* //console.log(mydata);
      const mydata = querySnapshot.docs.map((data,i)=>(
        <div key={i}>{data.data.name}</div>
      ))  */
      res(mydata);
  
    })
  })
  }



  const GetContact = async()=>{
    var docRef = db.collection(collectionName).doc('JDCn8UX8FjJzeTPRdLoT');

    //return new Promise((res, rej)=>{
    const mycourse =  await docRef.onSnapshot((querySnapshot)=> {
       //console.log(querySnapshot.docs[0])
      const mydata = querySnapshot.docs.map((item,i)=>(
        {
          id:item.id,
          name:item.data().name,
          email:item.data().email,
          phone:item.data().phone,
          address:item.data().address
        }
        //console.log(item.data())        
      ))   
     // var mydata = (<div>lets get loud</div>)
     console.log('data::',mydata);
     return mydata;
      
      //return mydata  
    })
    
    console.log('labas: ',mycourse)
    return mycourse;
    //console.log(Contact)
    //res(Contact) 
    
  //})
  }

  const watchContact = () =>{
  
  db.collection(collectionName)
    .onSnapshot((snapshot) =>{
        //...console
        const doc = snapshot.docChanges().map(change => ({
          
           id: change.doc.id,
          name: change.doc.data().name
        }));

        console.log(doc)
         

    }, (error)=> {
        //...
        console.log(error)
    });
  }


  export {Add, GetAll,GetAllSnap,GetContact,watchContact,Update};