import React from 'react';
import { connect } from "react-redux";
import {GetAll} from '../functions'
import {getDbContact} from '../../../../redux/actions'
import {Header, Contactlist,BottomTable, AddContactBtn} from '../Styles'
import {ClearFix} from '../../../common/ClearFIx'; 
import {addContacts as AddContacts} from './addContact';
import {Modal} from '../../../common/Modal/modal'
const mapDispatchToProps = dispatch => {
  return {
    //addContact: Contact => dispatch(addContact(Contact)),
    getDbContact: dbContact=>dispatch(getDbContact())
  };
};

const mapStateToProps = state => {
  return { dbContact: state.contact.dbContact
  };
};



class ListContact extends React.Component {

  constructor(props){
    super(props);

    props.getDbContact(GetAll())

    //console.log(GetAll());
    this.state={
      Contacts:this.props.dbContact,
      show:false,
    }
  }

  updateUser = (key,data)=>{
    this.showModal(key, data)
  }

  showModal = (key,data) => {
    //var me = this.state.ses[key].value = data;
    this.setState({ show: true, });

     //this.setState({me})
  };

  hideModal = (props) => {
    
    this.setState({ show: false });
    this.props.getDbContact(GetAll())
  };


render() {
 
return (
  <div>
    <Header><div>Contact Information</div></Header>         
    <Contactlist>
   <tbody>
    <tr >
        <td>Name:</td><td> {this.props.dbContact.name}</td>
    </tr>
    <tr>
        <td>Email:</td><td> {this.props.dbContact.email}</td>
    </tr>
    <tr>
        <td>Phone:</td><td> {this.props.dbContact.phone}</td>
        </tr>
    <tr>
        <td>Address:</td><td> {this.props.dbContact.address}</td>
     </tr>  
     </tbody>  
  
  </Contactlist>
  <BottomTable>

  <AddContactBtn onClick={()=>{this.updateUser(1,1)}}>Update Contact</AddContactBtn>
  </BottomTable>
  <ClearFix />

   <Modal show={this.state.show} handleClose={this.hideModal} title='Update User Details ' >
          
          {
            <AddContacts />
            // this.props.dbSessions && (this.props.dbSessions.map((itemData, i) =>(
            //  <CourseBtn onClick={()=>{this.selectedSession(this.state.selected,itemData.data.name)}}>{itemData.data.name} </CourseBtn>

            // )))
            }
          <p>Data</p>
        </Modal>
    </div>

    );
  }
}

const Contact = connect(mapStateToProps, mapDispatchToProps)(ListContact)
export default Contact;