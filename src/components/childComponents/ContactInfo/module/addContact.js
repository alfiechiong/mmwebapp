import React from 'react';
import {Update} from '../functions'
import {Form, Field} from 'react-final-form'
import {Required} from '../../../validator'
import Styles from '../Styles'

const addContact = e => {
    const data={
        name:e.name,
        email:e.email,
        phone:e.phone,
        address:e.address
    }
    Update(data) 
  };

const addContacts = (props)=>{

  const data = {name:'alfiechiong', email:'alfiechiong@gmail.com'}
    return (
        <Styles>
         <Form onSubmit={addContact} 
        // initialValues = {data}
         >
    {({ handleSubmit, values, submitting })=>(
      <form onSubmit={handleSubmit}>
      <Field 
      name='name' 
      placeholder='name' 
      validate={Required}
      >
      {({input, meta, placeholder}) =>(
       <div>
      <label>Name</label>
      <input {...input} placeholder={placeholder} />
      {meta.error && meta.touched && <span>{meta.error}</span>}
      </div>
      )}
      </Field>

       <Field 
      name='email' 
      placeholder='Email' 
      validate={Required}
      >
      {({input, meta, placeholder}) =>(
       <div>
      <label>Email</label>
      <input {...input} placeholder={placeholder} />
      {meta.error && meta.touched && <span>{meta.error}</span>}
      </div>
      )}
      </Field>

      <Field 
      name='phone' 
      placeholder='Phone' 
      validate={Required}
      >
      {({input, meta, placeholder}) =>(
       <div>
      <label>Phone</label>
      <input {...input} placeholder={placeholder} />
      {meta.error && meta.touched && <span>{meta.error}</span>}
      </div>
      )}
      </Field>

      <Field 
      name='address' 
      placeholder='Address' 
      validate={Required}
      >
      {({input, meta, placeholder}) =>(
       <div>
      <label>Address</label>
      <input {...input} placeholder={placeholder} />
      {meta.error && meta.touched && <span>{meta.error}</span>}
      </div>
      )}
      </Field>

      <button type='submit' disbaled={submitting}>Submit</button>

      </form>
    )}
    </Form>
        </Styles>
    )
}

export {addContacts}
