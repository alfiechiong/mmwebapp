import React from 'react';
import {Add,GetAll} from './functions';
class Project extends React.Component {
    constructor() {
        super();
        this.state = {
         name: '',
         description: '',
         image:'',
         projectduration:''
        };
      }

      componentWillMount = async ()=>{
        let projects = await GetAll();

        console.log(this.state.projects)

        let pro = projects.map((item, i)=>{
          return(
        <li key={i}>{item.data.Name}</li>
        )})
  
          this.setState({projects:pro});
        
        
      }

      updateInput = e => {
        this.setState({
          [e.target.name]: e.target.value
        });
      }

      addUser = e => {
        e.preventDefault();
        let data = {
        name:this.state.name,
        description: this.state.description,
        image:this.state.image,
        projectduration:this.state.projectduration
        }
         
        Add(data);
          
          this.setState({
            name: '',
            description: '',
            image:'',
            projectduration:''
          });
        
      };



  render() {

    
    return (
      <div>
        <form onSubmit={this.addUser}>
          <input
            type="text"
            name="name"
            placeholder="Name"
            onChange={this.updateInput}
            value={this.state.name}
          />
          <input
            type="text"
            name="projectduration"
            placeholder="Project duration"
            onChange={this.updateInput}
            value={this.state.projectduration}
          />
          <input
            type="text"
            name="image"
            placeholder="Image"
            onChange={this.updateInput}
            value={this.state.image}
          />
          <textarea
            name="description"
            placeholder="Description of the project"
            onChange={this.updateInput}
            value={this.state.description}
          />
          
          <button type="submit">Submit</button>
        
        </form>

        {
          this.state.projects
        }
        </div>
        );
      }
   }
export default Project;