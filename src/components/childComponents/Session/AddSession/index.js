import React, { Component } from "react";
//import firebase from "firebase";
import {storage} from '../../../../firebase';
import FileUploader from "react-firebase-file-uploader";
import {Audioplayer} from '../../../common/audioplayer';
import {Consumer} from '../../../AppProvider';
import {connect} from "react-redux";
import {Field, Form} from 'react-final-form';
import {Required} from '../../../validator'
import Styles from './Styles'
import {Add} from './functions';
import {GetAll} from '../functions'
import {addUploadedSession, getDbSessions} from '../../../../redux/actions'
const mapDispatchToProps = dispatch => {
  return {
    addSession: session => dispatch(addUploadedSession(session)),
    getDbSessions:dbSessions =>dispatch(getDbSessions('new me'))
  };
};


class Uploader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            avatar: "",
            isUploading: false,
            progress: 0,
            avatarURL: ""
          };

          //this.props.getDbSessions(this.getSessionsFromDb())
     
      }
  
      addSession = e => {
        const data= {
          name:e.name, 
          link:this.state.avatarURL,//e.link, 
          description:e.description
        }
    
        this.props.addSession(data);
          Add(data)
          this.props.history.push('/content');
      };

 
 
  handleChangeUsername = event =>
    this.setState({ username: event.target.value });
  handleUploadStart = () => this.setState({ isUploading: true, progress: 0 });
  handleProgress = progress => this.setState({ progress });
  handleUploadError = error => {
    this.setState({ isUploading: false });
    console.error(error);
  };
  handleUploadSuccess = filename => {
   // this.setState({ avatar: filename, progress: 100, isUploading: false });
    storage
      .ref("audio")
      .child(filename)
      .getDownloadURL()
      .then(
          url =>
            {
                //console.log(this.context);
               
               //  console.log(url)
                this.setState({ avatarURL: url,
                    avatar: filename, progress: 100, isUploading: false 
                 })  
                
            }
          );
  };

  getall = async () =>{
    console.log(await GetAll());
  }

  getSessionsFromDb = async () =>{
    return await GetAll()
  }
 
  render() {
    return (
        <Consumer>
             {({ state, ...context }) => (
      <Styles>          
        <button  onClick={()=>{
      this.props.getDbSessions(this.getSessionsFromDb())
    }}>Session Set</button>

     <button  onClick={()=>{
      this.getall(['sesssions me '])
    }}>getALL </button>
      <div>
        <Form onSubmit={this.addSession} >
{({ handleSubmit, values, submitting })=>(
        <form onSubmit={handleSubmit}>

           <Field 
              name='name' 
              placeholder='Name' 
              validate={Required}
              >
              {({input, meta, placeholder}) =>(
              <div>
              <label>Name</label>
              <input {...input} placeholder={placeholder} />
              {meta.error && meta.touched && <span>{meta.error}</span>}
              </div>
              )}
          </Field>

          <Field 
              name='description'
              placeholder='description'
              validate={Required}
              >
              {({input, meta, placeholder}) =>(
              <div>
              <label>Description</label>
              <textarea {...input} placeholder={placeholder} type='text' maxLength='1000'/>
              {meta.error && meta.touched && <span>{meta.error}</span>}
              </div>
              )}
          </Field>
                
          {this.state.isUploading && <p>Progress: {this.state.progress}</p>}
          {/* this.state.avatarURL && <img src={this.state.avatarURL} /> */ }
          {this.state.avatarURL  &&  <Audioplayer track={this.state.avatarURL} />}
         {/* this.state.avatarURL && context.setUploadedSessionUrl(this.state.avatarURL) */}
          <FileUploader
            accept="audio/*"
            name="avatar"
            //filename={file => this.state.username + file.name.split('.')[1]}
            randomizeFilename
            storageRef={storage.ref("audio")}
            onUploadStart={this.handleUploadStart}
            onUploadError={this.handleUploadError}
            onUploadSuccess={this.handleUploadSuccess}
            onProgress={this.handleProgress}
          />
          
          <button type='submit' disabled={submitting}>Submit</button>  
        </form>
        )}
        </Form>

      </div>
      </Styles >
             )}
      </Consumer>
    );
  }
}
 
export default connect(null,mapDispatchToProps)(Uploader);