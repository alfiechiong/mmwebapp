import React from 'react';
import {Add,getSessions, updateSession,getFreeSessions} from './functions';
import {
  Link
} from 'react-router-dom';
import {Form, Field} from 'react-final-form'
import {Required} from '../../validator'
import { connect } from "react-redux";
import Styles,{SubmitBtn} from './Styles'
import Uploader from '../../common/uploader'
import {Audioplayer} from '../../common/audioplayer';
import AudioPlayer from '../../common/audio/AudioPlayer';
import {GetAll} from '../../childComponents/Session/functions'
import {addUploadedSession,getDbFreeSessions} from '../../../redux/actions'
import {SessionList} from './ListSessions';
import {ClearFix} from '../../common/ClearFIx'
import {Header,FreeSessionlist} from './Styles'
import { fromJSON } from 'tough-cookie';
import {Modal} from '../../common/Modal/modal';
import {PowerUpgradeComp} from '../../common/powerUps'
const mapDispatchToProps = dispatch => {
  return {
    addSession: session => dispatch(addUploadedSession(session)),
    getDbFreeSessions: dbFreeSessions=>dispatch(getDbFreeSessions())
  };
};

const mapStateToProps = state => {
  return { dbFreeSessions: state.session.dbFreeSessions[0]
  };
};


class OneMinuteSession extends React.Component {

  constructor(props){
    super(props);
    props.getDbFreeSessions()

    this.state = {
      show:false,
      selected:undefined,
      selectedName:undefined,
      selectedDesc:undefined,
      selectedId:undefined,
      selectedLink:undefined
    }
  }

  showModal = (key,data) => {
   
    this.setState({ show: true, selected:key});
    this.setData(key)
     //this.setState({me})
  };

  hideModal = () => {
    this.setState({ show: false });
    window.location.reload();
  };

  handleSubmit = (e) =>{

    const data = {
      id:this.state.selectedId,
      name:this.refs.name.value,
      link:this.state.selectedLink,
      description:this.refs.description.value
    }
    console.log(data);
    updateSession(data)
   

  } 
  setData = (key) =>{
    const name = this.props.dbFreeSessions[key].data.name
    const desc = this.props.dbFreeSessions[key].data.description
    const link = this.props.dbFreeSessions[key].data.link
    const id = this.props.dbFreeSessions[key].dataid

    this.setState({
      selectedName:name,
      selectedDesc:desc,
      selectedId:id,
      selectedLink:link
    })    

    this.refs.description.value = desc;
    this.refs.name.value = name;
    this.refs.link.value = link;
  }

render() {
return (
  <div>
    <div>
       
    <FreeSessionlist>

    <Header>Change Your Perspective</Header>  
    <ul>
    <div> {this.props.dbFreeSessions && this.props.dbFreeSessions[0].data.name}</div>
    <li>
    
    <br />
    <br />
    <br />
    <div> </div>
    
    </li>
    <AudioPlayer track={this.props.dbFreeSessions && this.props.dbFreeSessions[0].data.link} />
   
    <h3>Description</h3>
    <div>{this.props.dbFreeSessions && this.props.dbFreeSessions[0].data.description}</div>
    
    
  </ul>
    </FreeSessionlist>
    <PowerUpgradeComp />
  <ClearFix />
  </div>

  </div>
    );
  }
}

const Ses = connect(mapStateToProps, mapDispatchToProps)(OneMinuteSession)
export default Ses;