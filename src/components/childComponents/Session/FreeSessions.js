import React from 'react';
import { updateSession} from './functions';
import { connect } from "react-redux";
import Styles,{SubmitBtn} from './Styles'
import {Audioplayer} from '../../common/audioplayer';
import {addUploadedSession,getDbFreeSessions} from '../../../redux/actions'
import {ClearFix} from '../../common/ClearFIx'
import { FreeSessionlist} from './Styles'
import {Modal} from '../../common/Modal/modal';

const mapDispatchToProps = dispatch => {
  return {
    addSession: session => dispatch(addUploadedSession(session)),
    getDbFreeSessions: dbFreeSessions=>dispatch(getDbFreeSessions())
  };
};

const mapStateToProps = state => {
  return { dbFreeSessions: state.session.dbFreeSessions[0]
  };
};


class Session extends React.Component {

  constructor(props){
    super(props);
    props.getDbFreeSessions()

    this.state = {
      show:false,
      selected:undefined,
      selectedName:undefined,
      selectedDesc:undefined,
      selectedId:undefined,
      selectedLink:undefined
    }
  }

  showModal = (key,data) => {
   
    this.setState({ show: true, selected:key});
    this.setData(key)
     //this.setState({me})
  };

  hideModal = () => {
    this.setState({ show: false });
    window.location.reload();
  };

  handleSubmit = (e) =>{

    const data = {
      id:this.state.selectedId,
      name:this.refs.name.value,
      link:this.state.selectedLink,
      description:this.refs.description.value
    }
    console.log(data);
    updateSession(data)
   

  } 
  setData = (key) =>{
    const name = this.props.dbFreeSessions[key].data.name
    const desc = this.props.dbFreeSessions[key].data.description
    const link = this.props.dbFreeSessions[key].data.link
    const id = this.props.dbFreeSessions[key].dataid

    this.setState({
      selectedName:name,
      selectedDesc:desc,
      selectedId:id,
      selectedLink:link
    })    

    this.refs.description.value = desc;
    this.refs.name.value = name;
    this.refs.link.value = link;
  }

render() {
return (
  <div>
    <div>
       
    <FreeSessionlist>
    <ul>
    {  this.props.dbFreeSessions && (this.props.dbFreeSessions.map((itemData, i) =>(
    <li key={i}>
    
    <div> {itemData.data.name}</div>
    <br />
    <div> <Audioplayer track={itemData.data.link} /></div>
    <h3>Description</h3>
    <div>{itemData.data.description}</div>
    </li>
    
  ))) }
  </ul>
    </FreeSessionlist>
    
  
  <ClearFix />
  </div>
 
   <Modal show={this.state.show} handleClose={this.hideModal} title='Update Session ' >
          {
            <Styles>
           <div>
             {this.state.selectedDesc}
             <form>
                <label> Name</label>
                 <input type='text' name='name' ref='name'/>

                  <label> Desc</label>
                   <textarea name='ss' ref='description'  />

                   <label> Link</label>
                 <input type='text' name='link' ref='link' disabled/>

                 <Audioplayer track={this.state.selectedLink} />
                 
              
             </form>
             <SubmitBtn onClick={()=>{this.handleSubmit()}}>submit</SubmitBtn>
             </div>
             </Styles>
            }
       
        </Modal>
  </div>
    );
  }
}

const Ses = connect(mapStateToProps, mapDispatchToProps)(Session)
export default Ses;