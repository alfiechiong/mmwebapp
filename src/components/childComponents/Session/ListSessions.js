import React from 'react';
import {Add,getSessions, updateSession} from './functions';
import {
  Link
} from 'react-router-dom';
import {Form, Field} from 'react-final-form'
import {Required} from '../../validator'
import { connect } from "react-redux";
import Styles,{SubmitBtn} from './Styles'
import Uploader from '../../common/uploader'
import {Audioplayer} from '../../common/audioplayer';
import {GetAll} from '../../childComponents/Session/functions'
import {addUploadedSession,getDbSessions} from '../../../redux/actions'
import {SessionList} from './ListSessions';
import {ClearFix} from '../../common/ClearFIx'
import {Sessionlist,Header, BottomTable, AddSessionBtn,Pagination, EditSession} from './Styles'
import { fromJSON } from 'tough-cookie';
import {Modal} from '../../common/Modal/modal';

const mapDispatchToProps = dispatch => {
  return {
    addSession: session => dispatch(addUploadedSession(session)),
    getDbSessions: dbSessions=>dispatch(getDbSessions(['thehehyhuhw']))
  };
};

const mapStateToProps = state => {
  return { dbSessions: state.session.dbSessions[0]
  };
};


class Session extends React.Component {

  constructor(props){
    super(props);
    props.getDbSessions()

    this.state = {
      show:false,
      selected:undefined,
      selectedName:undefined,
      selectedDesc:undefined,
      selectedId:undefined,
      selectedLink:undefined
    }
  }

  showModal = (key,data) => {
   
    this.setState({ show: true, selected:key});
    this.setData(key)
     //this.setState({me})
  };

  hideModal = () => {
    this.setState({ show: false });
    window.location.reload();
  };

  handleSubmit = (e) =>{

    const data = {
      id:this.state.selectedId,
      name:this.refs.name.value,
      link:this.state.selectedLink,
      description:this.refs.description.value
    }
    console.log(data);
    updateSession(data)
   

  } 
  setData = (key) =>{
    const name = this.props.dbSessions[key].data.name
    const desc = this.props.dbSessions[key].data.description
    const link = this.props.dbSessions[key].data.link
    const id = this.props.dbSessions[key].dataid

    this.setState({
      selectedName:name,
      selectedDesc:desc,
      selectedId:id,
      selectedLink:link
    })    

    this.refs.description.value = desc;
    this.refs.name.value = name;
    this.refs.link.value = link;
  }

render() {
return (
  <div>
    <div>
    <Header><div>Sessions</div></Header>         
    <Sessionlist>
      <table>
      <tbody>
      <tr><th>Name</th>
      <th>Action</th>
      </tr>
  {  this.props.dbSessions && (this.props.dbSessions.map((itemData, i) =>(
    <tr key={i}>
    <td> {itemData.data.name}</td>
    <td><EditSession onClick={()=>this.showModal(i)} id={itemData.dataid}>EDIT</EditSession></td>
    </tr>
  ))) }
  </tbody>
  </table>
  </Sessionlist>
  <BottomTable>
  <Pagination>  </Pagination>
  <AddSessionBtn><Link to='/addsessions'>Add New Session</Link></AddSessionBtn>
  </BottomTable>
  <ClearFix />
  </div>
 
   <Modal show={this.state.show} handleClose={this.hideModal} title='Update Session ' >
          {
            <Styles>
           <div>
             {this.state.selectedDesc}
             <form>
                <label> Name</label>
                 <input type='text' name='name' ref='name'/>

                  <label> Desc</label>
                   <textarea name='ss' ref='description'  />

                   <label> Link</label>
                 <input type='text' name='link' ref='link' disabled/>

                 <Audioplayer track={this.state.selectedLink} />
                 
              
             </form>
             <SubmitBtn onClick={()=>{this.handleSubmit()}}>submit</SubmitBtn>
             </div>
             </Styles>
            }
       
        </Modal>
  </div>
    );
  }
}

const Ses = connect(mapStateToProps, mapDispatchToProps)(Session)
export default Ses;