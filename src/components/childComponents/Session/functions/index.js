import {db} from '../../../../firebase';


  const collectionName = 'Sessions';
  const Add = (data) => {
   
      db.collection(collectionName).add({
        name: data.name,
        link: data.link,
        description:data.description
      }).then((docRef) => {
        console.log("Document written with ID: ", docRef.id);
        })
            .catch((error)=> {
            console.error("Error adding document: ", error);
    });
  };

  const updateSession = (data) =>{
    console.log(data.id)
    db.collection(collectionName).doc(data.id).set({
      name: data.name,
      link: data.link,
      description:data.description
    }).then(()=>{
      alert('success')
    })
  }

  const getSessions = async()=>{
    const ref = db.collection(collectionName).limit(10)

    return new Promise((res, rej)=>{
    ref.orderBy("name")
    .onSnapshot((snapshot) => {
        //return data
        //console.log("Current data: ", doc.data());
    
        const data = snapshot.docs.map((item,i)=>{
          return {
            dataid:item.id,
            data:item.data()
          }
        })

        res(data)
    })
  
})
  }

const getFreeSessions = async()=>{
 // const random = this.afs.createId();
    const ref = db.collection(collectionName).limit(3)

    return new Promise((res, rej)=>{
    ref.orderBy("name")
    .onSnapshot((snapshot) => {
        //return data
        //console.log("Current data: ", doc.data());
    
        const data = snapshot.docs.map((item,i)=>{
          return {
            dataid:item.id,
            data:item.data()
          }
        })

        res(data)
    })
  
})
  }


  const getAllSessions = async()=>{
    const ref = db.collection(collectionName);

    return new Promise((res, rej)=>{
    ref.onSnapshot((snapshot) => {
        //return data
        //console.log("Current data: ", doc.data());
    
        const data = snapshot.docs.map((item,i)=>{
          return {
            dataid:item.id,
            data:item.data()
          }
        })

        res(data)
    })
  
})
  }


  const GetAll = ()=>{
    var docRef = db.collection(collectionName);

    return new Promise((res, rej)=>{
    docRef.get().then((querySnapshot)=> {
      const mydata = querySnapshot.docs.map((data)=>({
        id:data.id,
        data:data.data()
      }))

      //console.log(mydata);
      res(mydata);
  
    }).catch(function(error) {
      rej(error)
        console.log("Error getting document:", error);
    });
  })
  }

  export {Add, GetAll, getSessions,updateSession,getAllSessions,getFreeSessions}