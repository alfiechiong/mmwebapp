import React from 'react';
import {Add} from './functions';
import {Form, Field} from 'react-final-form'
import {Required, Email, Phone} from '../../validator'


class User extends React.Component {
    constructor() {
        super();
        this.state = {
         email: '',
         fullname: ''
        };
      }

      updateInput = e => {
        this.setState({
          [e.target.name]: e.target.value
        });

        console.log(this.state.fullname)
      }

      addUser = e => {
        //e.preventDefault();
        //console.log(e);
         
        Add(e.fullname, e.email, e.phone)
          
          /* this.setState({
          fullname: '',
          email: ''
          }); */
        
      };

  //required = value=>value ? undefined:'required';
  render() {
    return (
      <Form onSubmit={this.addUser} >
    {({ handleSubmit, values, submitting })=>(
      <form onSubmit={handleSubmit}>
      <Field 
      name='fullname' 
      placeholder='fullname' 
      validate={Required}
      >
      {({input, meta, placeholder}) =>(
       <div>
      <label>Name</label>
      <input {...input} placeholder={placeholder} />
      {meta.error && meta.touched && <span>{meta.error}</span>}
      </div>
      )}
      </Field>

      <Field 
      name='email' 
      placeholder='email' 
      validate={Email}
      >
      {({input, meta, placeholder}) =>(
       <div>
      <label>Email</label>
      <input {...input} placeholder={placeholder} type='email'/>
      {meta.error && meta.touched && <span>{meta.error}</span>}
      </div>
      )}
      </Field>

      <Field 
      name='phone' 
      placeholder='phone' 
      validate={Phone}
      >
      {({input, meta, placeholder}) =>(
       <div>
      <label>Phone</label>
      <input {...input} placeholder={placeholder} type='number'/>
      {meta.error && meta.touched && <span>{meta.error}</span>}
      </div>
      )}
      </Field>

      <Field 
      name='comment' 
      placeholder='Comment' 
      validate={Required}
      >
      {({input, meta, placeholder}) =>(
       <div>
      <label>Comment</label>
      <textarea {...input} placeholder={placeholder} type='text' maxlength='1000'/>
      {meta.error && meta.touched && <span>{meta.error}</span>}
      </div>
      )}
      </Field>

      <button type='submit' disbaled={submitting}>Submit</button>

      </form>
    )}
    </Form>
      
       /*  <form onSubmit={this.addUser}>
          <input
            type="text"
            name="fullname"
            placeholder="Full name"
            onChange={this.updateInput}
            value={this.state.fullname}
          />
          <input
            type="email"
            name="email"
            placeholder="Full name"
            onChange={this.updateInput}
            value={this.state.email}
          />
          <button type="submit">Submit</button>
        </form> */
        );
      }
   }
export default User;