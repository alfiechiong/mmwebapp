import React from 'react';
import { connect } from "react-redux";
import {ClearFix} from '../../common/ClearFIx'
import {Header, BottomTable, AddSessionBtn,Pagination,Userlist} from './Styles'
import {GetAllWithLimit} from './functions'
import {GetAllUsers} from '../../pages/dashboard/functions'
import {getDbUsers} from '../../../redux/actions'
import UserIcon from  '../../../assets/users.svg'


const mapDispatchToProps = dispatch => {
  return {
  //  addSession: session => dispatch(addUploadedSession(session)),
    getDbUsers: dbUsers=>dispatch(getDbUsers())
  };
};

const mapStateToProps = state => {
  return { dbUsers: state.user.dbUsers[0]
  };
};

class Users extends React.Component {

  constructor(props){
    super(props);

    props.getDbUsers(this.getUsersFromDb())

    this.state={
      users:this.props.dbUsers,
      user:[]
    }
  }
 
  getUsersFromDb = async () =>{
   // return await GetAllWithLimit(4)
  }

  componentDidMount =  async() => {
   this.setState({user:[...this.state.user, await GetAllUsers()]});
    console.log('conso ', await GetAllUsers())
  }

  renderUser = async () =>{
   //return await this.state.user;
  }
  
 render(){
  console.log('00',this.state.user)
return (
  <div>
    <div>
   
    <Header><img src={UserIcon} width='40' height='40'/><div>USERS</div></Header>      
    
    <Userlist>
      <tbody>
      <tr><th>Name</th></tr>
  { 
    //for async 
    /* this.state.user && this.state.user.map((item , i)=>{
      return(
       <tr key={i}>
        <td>{item}</td>
       </tr>
      )

    }) */
    
    //this.state.user
      this.props.dbUsers && (this.props.dbUsers.map((itemData, i) =>(
    <tr key={itemData.id}>
    <td> {itemData.data.name}</td>
    </tr>
  )))  }
  </tbody>
  </Userlist>
  <BottomTable>
  <Pagination> </Pagination>

  </BottomTable>
  <ClearFix />
  </div>
  </div>
    );
  }
}

const User = connect(mapStateToProps, mapDispatchToProps)(Users)
export default User;
