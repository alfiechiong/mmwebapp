import {db} from '../../../../firebase';

/* const Add = (fullname, email) => {
    if(fullname !== null && fullname !== '' && email !== null && email !== '')
    {
      db.collection('Users').add({
        fullname: fullname,
        email: email
      }).then((docRef) => {
        console.log("Document written with ID: ", docRef.id);
        })
            .catch((error)=> {
            console.error("Error adding document: ", error);
    });
    }else{
        alert( 'all fields cannot be empty');
    }
  }; */

  const collectionName = 'Users';

  const Add = (fullname, email) => {
    if(fullname !== null && fullname !== '' && email !== null && email !== '')
    {
      db.collection('Users').add({
        fullname: fullname,
        email: email
      }).then((docRef) => {
        console.log("Document written with ID: ", docRef.id);
        })
            .catch((error)=> {
            console.error("Error adding document: ", error);
    });
    }else{
        alert( 'all fields cannot be empty');
    }
  };

  const GetAll = ()=>{
    var docRef = db.collection(collectionName);

    return new Promise((res, rej)=>{
    docRef.get().then((querySnapshot)=> {
      const mydata = querySnapshot.docs.map((data)=>({
        id:data.id,
        data:data.data()
      }))
      //console.log(mydata)
      res(mydata);
  
    }).catch(function(error) {
      rej(error)
        console.log("Error getting document:", error);
    });
  })
  }

  const GetAllWithLimit = ()=>{
    var docRef = db.collection(collectionName).where("name","==","alfiechiong" ).get();

    return new Promise((res, rej)=>{
    docRef.then((querySnapshot)=> {
      const mydata = querySnapshot.docs.map((data)=>({
        id:data.id,
        data:data.data()
      }))
      console.log(mydata)
      res(mydata);
  
    }).catch(function(error) {
      rej(error)
        console.log("Error getting document:", error);
    });
  })
  }

  const GetCurrentUser = (email)=>{
    var docRef = db.collection('users').where("name","==",email).get();

    return new Promise((res, rej)=>{
    docRef.then((querySnapshot)=> {
      const mydata = querySnapshot.docs.map((data)=>({
        id:data.id,
        data:data.data()
      }))
      console.log(mydata)
      res(mydata);
  
    }).catch(function(error) {
      rej(error)
        console.log("Error getting document:", error);
    });
  })
  }



  export {Add, GetAll,GetCurrentUser}