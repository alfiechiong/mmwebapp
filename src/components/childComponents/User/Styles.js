import styled, { css } from 'styled-components'

export const Header = styled.div`
background-image: linear-gradient(to right, #00516a, #00a89c);
  border-top-right-radius: 10px;
  border-top-left-radius: 10px;
  height:30px;
  padding:10px;

  >img {
    float:left;
  }

  &>div{
      margin-top:5px;
      width: 123px;
      height: 17px;
      font-family: Montserrat;
      font-size: 24px;
      font-weight: 600;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: normal;
      text-align: left;
      color: #ffffff;
      float:left;
    
  }
`
export const Userlist = styled.table`
    border-collapse: collapse;
    width: 100%;
    clear:both;
    overflow:auto;
    border:solid thin #65c7c3 ;
    border-top:none;
    box-shadow: 4.9px 4.9px 0 0 rgba(207, 207, 209, 0.75);
  }

    & > th, td {
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even){background-color: #f2f2f2}

    th {
      font-family: Montserrat;
      font-size: 22px;
      font-weight: 600;
      text-align:left;
      padding-left:10px;
    }
    `


    export const BottomTable = styled.div`
    margin-top:20px;
    padding:10px;
   

    &>div{
     
      width:100%;

    }
  `

export const Pagination = styled.div`
height: 40px;
padding:10px;
`  
export const AddSessionBtn = styled.div`
width: 346px;
height: 40px;
padding-top:10px;
box-shadow: 2.8px 2.8px 0 0 rgba(207, 207, 209, 0.75);
background-color:#2e4f80;
border-radius:10px;
text-align:center;
  font-family: Montserrat;
  font-size: 20px;
  font-weight: 600;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #ffffff;

`    
