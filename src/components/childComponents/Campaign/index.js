import React from 'react';
import {Add,GetAll} from './functions';
class Campaign extends React.Component {
    constructor() {
        super();
        this.state = {
         name: '',
         description: '',
         image:'',
         Campaignduration:''
        };
      }

      componentWillMount = async ()=>{
        let Campaigns = await GetAll();

        console.log(this.state.Campaigns)

        let pro = Campaigns.map((item, i)=>{
          return(
        <li key={i}>{item.data.Name}</li>
        )})
  
          this.setState({Campaigns:pro});
        
        
      }

      updateInput = e => {
        this.setState({
          [e.target.name]: e.target.value
        });
      }

      addUser = e => {
        e.preventDefault();
        let data = {
        name:this.state.name,
        description: this.state.description,
        image:this.state.image,
        Campaignduration:this.state.Campaignduration
        }
         
        Add(data);
          
          this.setState({
            name: '',
            description: '',
            image:'',
            Campaignduration:''
          });
        
      };



  render() {

    
    return (
      <div>
        <form onSubmit={this.addUser}>
          <input
            type="text"
            name="name"
            placeholder="Name"
            onChange={this.updateInput}
            value={this.state.name}
          />
          <input
            type="text"
            name="Campaignduration"
            placeholder="Campaign duration"
            onChange={this.updateInput}
            value={this.state.Campaignduration}
          />
          <input
            type="text"
            name="image"
            placeholder="Image"
            onChange={this.updateInput}
            value={this.state.image}
          />
          <textarea
            name="description"
            placeholder="Description of the Campaign"
            onChange={this.updateInput}
            value={this.state.description}
          />
          
          <button type="submit">Submit</button>
        
        </form>

        {
          this.state.Campaigns
        }
        </div>
        );
      }
   }
export default Campaign;