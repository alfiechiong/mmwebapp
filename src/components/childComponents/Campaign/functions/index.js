import {db} from '../../../../firebase';
const collectionName = 'Campaign';
const Add = (data ) => {

      db.collection(collectionName).add({
        Name: data.name,
        Description: data.description,
        Image:data.image,
        ProjectDuration:data.projectduration,
        startDate:Date()
      }).then((docRef) => {
        console.log("Project Added with ID: ", docRef.id);
        })
            .catch((error)=> {
            console.error("Error adding document: ", error);
    });
   
  };

  const GetByDocId = (docid)=>{
    var docRef = db.collection(collectionName).doc(docid);

    docRef.get().then(function(doc) {
        if (doc.exists) {
            console.log("Document data:", doc.data());
        } else {
            // doc.data() will be undefined in this case
            console.log("No such document!");
        }
    }).catch(function(error) {
        console.log("Error getting document:", error);
    });
  }

  const GetAll = ()=>{
    var docRef = db.collection(collectionName);

    return new Promise((res, rej)=>{
    docRef.get().then((querySnapshot)=> {
      const mydata = querySnapshot.docs.map((data)=>({
        id:data.id,
        data:data.data()
      }))

      //console.log(mydata);
      res(mydata);
  
    }).catch(function(error) {
      rej(error)
        console.log("Error getting document:", error);
    });
  })
  }

  export {Add, GetByDocId,GetAll}