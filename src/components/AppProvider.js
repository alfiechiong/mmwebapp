import React, {
    Component,
    createContext
  } from 'react';
  import { firebase,db } from '../firebase'
  import {connect} from 'react-redux'
import {setCurrentUser} from '../redux/actions'

//import { dispatch } from 'rxjs/internal/observable/range';
  export const {
    Provider,
    Consumer
  } = createContext();
  
  const mapDispatchToProps = dispatch => {
    return {
    setCurrentUser: data => dispatch(setCurrentUser(data))
   
    };
  };

  class AppProvider extends Component {
    state = {
      currentUser: AppProvider.defaultProps.currentUser,
      email:null,
      type:undefined,
      message: AppProvider.defaultProps.message,
      alfie:AppProvider.defaultProps.alfie,
      uploadedSessionUrl: null,
      currentLocation:'dashboard',
      droppedFiles:null,
      userDocId:undefined,
      AudioPlay:false
    }

  

  
    componentDidMount = async () => {
     
      firebase.auth.onAuthStateChanged(user => 
        {
        user && this.setState({
        currentUser: user,
        email:user.email

      })

      this.userType(user.email)
    }
      )
    }

   userType = (email)=>{
      const ref = db.collection('users').where('email','==',email);
       ref.onSnapshot(snap=>{
          //console.log(snap.docs[0].data().type)
          let type = (snap.docs[0] !== undefined ? snap.docs[0].data().type: '' )
          let id = (snap.docs[0] !== undefined ? snap.docs[0].id: '' )
          this.setState({type})

          this.props.setCurrentUser({email:email,type, userDocId:id})
         // return snap.docs[0].data().type
      })
  }
  
    render() {
      return (
        <Provider value={{
          state: this.state,
          destroySession: () => this.setState({ 
            currentUser: AppProvider.defaultProps.currentUser
          }),
          setMessage: message => this.setState({ message }),
          clearMessage: () => this.setState({ 
            message: AppProvider.defaultProps.message 
          }),
          setUploadedSessionUrl: url=>{
            this.setState({uploadedSessionUrl:url})
          },
          setCurrentLocation: location=>{
                this.setState({currentLocation:location})  
          },
          setDroppedFiles: files=>{
            this.setState({droppedFiles:files})  
        },
        setAudioPlay:()=>{
          this.setState({AudioPlay:true})  
        },
        setAudioStop:()=>{
          this.setState({AudioPlay:false})  
        }
        }}>
          {this.props.children}
        </Provider>
      )
    }
  }
  
  AppProvider.defaultProps = {
    currentUser: null,
    message: null
  }
  
  export default connect(null,mapDispatchToProps)(AppProvider);