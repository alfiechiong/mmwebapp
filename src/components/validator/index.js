const Required = value=>value ? undefined:'required';

const Email = value=>{
    if((/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value)))
    {
        return undefined
    }else{
        return 'invalid email'
    }
};

const Phone = value=>{
    var phoneno = new RegExp(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/);
  if(phoneno.test(value))
  {
    return undefined
    }else{
    return 'invalid ... 10 digit number are only allowed';
    }
}


export {Required, Email, Phone}



