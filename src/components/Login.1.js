import React from 'react';
import { Link,withRouter } from 'react-router-dom';
import Form from '../shared/Form';
import { Consumer } from './AppProvider';
//import Button from '@material-ui/core/Button';
import { Button } from 'reactstrap';
import { CardActionArea, Grid, Card } from '@material-ui/core';
import Authentication from './childComponents/Authentication';

const Login = props => <Consumer>
  {({ state, ...context }) => (
     <Grid container justify="center">
      <Card>
    <Form
      action="signIn"
      title="Login"
      onSuccess={() => props.history.push('/me')}
      onError={({ message }) => context.setMessage(`Login failed: ${message}`)}
    />
    <Button><Link to="/signup">Create Account</Link></Button> 
    </Card>

    <h3>Authentication</h3>
           <Authentication />
    </Grid>
  )}
  
</Consumer>;
//export default Login;
export default withRouter(Login);