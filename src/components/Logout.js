import React from 'react';
import { withRouter } from 'react-router-dom';
import styled from 'styled-components';

const windowHeight = window.innerHeight + 'px';

export const Styles = styled.div`
height:${windowHeight};
display:flex;

&>div{
    border:solid thin #ccc;
    border-radius:10px;
    padding:20px;
    text-align:center;
    margin:auto;

    max-width:300px;
    justify-content:center;

    button {
        margin-top:20px;
        padding:10px;
        border-radius:5px;
        font-size:12px;
    }
}
`

const logout = props => 
<Styles>
<div>
<div>THANK YOU,

    SIGNED OUT
</div>
<button onClick={()=>{window.location.href = '/'}}>Do you Want to Login Again</button>
</div>
</Styles>

  
export default withRouter(logout);

