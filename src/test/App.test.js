import React from 'react';
import Enzyme, {shallow} from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import App from '../components/App';

Enzyme.configure({adapter:new EnzymeAdapter()});
const wrapper = shallow(<App />)

it('renders with dataattr and to be true', () => {
    const comp = wrapper.find("[test-data='tesing']");
    expect(wrapper).toBeTruthy();
    expect(comp.length).toBe(1);
    
})

it('check for the child count component',()=>{
    expect(wrapper.children().length).toBe(1);
    expect(wrapper.children())
})

it('check for Myrouter child exist', ()=>{
    //expect(wrapper.childAt(0).name()).toEqual('AppProvider');
   // console.log(wrapper.debug());
   // wrapper.update();
})