import * as auth from './auth';
import * as firebase from './firebase';
//import 'firebase/storage'
//import {storage} from '@google-cloud/storage';

const db = firebase.db();
const storage = firebase.storage();
db.settings({ timestampsInSnapshots: true });
export {
  auth,
  firebase,
  db,
  storage
}