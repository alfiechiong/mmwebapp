const devConfig = {
  apiKey: "AIzaSyC3pEGIo1isR02s16DJ-yV76zAio9PRab4",
  authDomain: "mindsetmaestro-c57eb.firebaseapp.com",
  databaseURL: "https://mindsetmaestro-c57eb.firebaseio.com",
  projectId: "mindsetmaestro-c57eb",
  storageBucket: "mindsetmaestro-c57eb.appspot.com",
  messagingSenderId: "1080644339373"
  };
  
  const prodConfig = {
    apiKey: "AIzaSyC3pEGIo1isR02s16DJ-yV76zAio9PRab4",
    authDomain: "mindsetmaestro-c57eb.firebaseapp.com",
    databaseURL: "https://mindsetmaestro-c57eb.firebaseio.com",
    projectId: "mindsetmaestro-c57eb",
    storageBucket: "mindsetmaestro-c57eb.appspot.com",
    messagingSenderId: "1080644339373"
  };
  
  export {
    devConfig,
    prodConfig
  }