import * as firebase from 'firebase';
import 'firebase/storage'
import { devConfig } from './config';

!firebase.apps.length && firebase.initializeApp(devConfig)

const auth = firebase.auth();
const db = firebase.firestore;
const storage = firebase.storage;

export {
  auth,
  db,
  storage
}
