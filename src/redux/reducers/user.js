import {  
  GET_USERS,
  GET_USERS_FAILURE,
  GET_USERS_SUCCESS,
  GET_USERS_FETCHING,
  GET_DB_CURRENT_USER,
  GET_CURRENT_USER_FAILURE,
  GET_CURRENT_USER_FETCHING,
  GET_CONTACT_SUCCESS,
  GET_CURRENT_USER_SUCCESS,
  SET_CURRENT_USER
} from "../constants/action-types";
const initialState = {
  User:'',
  dbUsers:[],
  userCount:0,
  dbUserFetching:false,
  dbUserSuccess:false,
  dbUserFailure:false,
  dbCurrentUserFetching:false,
  dbCurrentUserSuccess:false,
  dbCurrentUserFailure:false,
  currentUser:''
};
const user = (state = initialState, action) => {
  switch (action.type) {

  case SET_CURRENT_USER:
    return { ...state, currentUser:action.payload}; 
  
  case GET_DB_CURRENT_USER:
    return { ...state, User:action.payload}; 
  
  case GET_CURRENT_USER_FETCHING:
    return { ...state, dbCurrentUserFetching:  action.payload }; 

  case GET_CURRENT_USER_SUCCESS:
    return { ...state, dbCurrentUserSuccess: action.payload }; 

  case GET_CURRENT_USER_FAILURE:
    return { ...state, dbCurrentUserFailure:  action.payload }; 


      case GET_USERS:
        return { ...state, dbUsers: [...state.dbUsers, action.payload] , userCount:action.payload.length }; 
      
      case GET_USERS_FETCHING:
        return { ...state, dbUserFetching:  action.payload }; 

      case GET_USERS_SUCCESS:
        return { ...state, dbUserSuccess: action.payload }; 

      case GET_USERS_FAILURE:
        return { ...state, dbUserFailure:  action.payload }; 
    default:
      return state;
  }
};
export default user;