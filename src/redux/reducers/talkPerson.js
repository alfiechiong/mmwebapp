import {  
  GET_DB_TALKPERSONS,
  GET_TALKPERSON_FAILURE,
  GET_TALKPERSON_SUCCESS,
  GET_TALKPERSON_FETCHING 
} from "../constants/action-types";
const initialState = {
  TalkPerson:[],
  dbTalkPersons:[],
  dbTalkPersonFetching:false,
  dbTalkPersonSuccess:false,
  dbTalkPersonFailure:false
};
const TalkPerson = (state = initialState, action) => {
  switch (action.type) {

     case GET_DB_TALKPERSONS:
        return { ...state, dbTalkPersons: [...state.dbTalkPersons, action.payload] }; 

      case GET_TALKPERSON_FETCHING:
        return { ...state, dbTalkPersonFetching:  action.payload }; 

      case GET_TALKPERSON_SUCCESS:
        return { ...state, dbTalkPersonSuccess: action.payload }; 

      case GET_TALKPERSON_FAILURE:
        return { ...state, dbTalkPersonFailure:  action.payload }; 
    default:
      return state;
  }
};
export default TalkPerson;