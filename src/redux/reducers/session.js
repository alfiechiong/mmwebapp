import {  
  ADD_UPLOADED_SESSION,
  GET_DB_SESSIONS,
  GET_SESSION_FAILURE,
  GET_SESSION_SUCCESS,
  GET_SESSION_FETCHING,
  GET_DB_FREE_SESSIONS,
  GET_FREE_SESSION_FAILURE,
  GET_FREE_SESSION_SUCCESS,
  GET_FREE_SESSION_FETCHING  
} from "../constants/action-types";
const initialState = {
  session:[],
  dbSessions:[],
  dbSessionFetching:false,
  dbSessionSuccess:false,
  dbSessionFailure:false,
  dbFreeSessions:[],
  dbFreeSessionFetching:false,
  dbFreeSessionSuccess:false,
  dbFreeSessionFailure:false,

};
const session = (state = initialState, action) => {
  switch (action.type) {
    case ADD_UPLOADED_SESSION:
        return { ...state, session: [...state.session, action.payload] };
     case GET_DB_SESSIONS:
        return { ...state, dbSessions: [...state.dbSessions, action.payload] }; 

      case GET_SESSION_FETCHING:
        return { ...state, dbSessionFetching:  action.payload }; 

      case GET_SESSION_SUCCESS:
        return { ...state, dbSessionSuccess: action.payload }; 

      case GET_SESSION_FAILURE:
        return { ...state, dbSessionFailure:  action.payload }; 
      
        case GET_DB_FREE_SESSIONS:
        return { ...state, dbFreeSessions: [...state.dbFreeSessions, action.payload] }; 

      case GET_FREE_SESSION_FETCHING:
        return { ...state, dbFreeSessionFetching:  action.payload }; 

      case GET_FREE_SESSION_SUCCESS:
        return { ...state, dbFreeSessionSuccess: action.payload }; 

      case GET_FREE_SESSION_FAILURE:
        return { ...state, dbFreeSessionFailure:  action.payload }; 
    default:
      return state;
  }
};
export default session;