import { SET_AGREEMENT } from "../constants/action-types";
const initialState = {
  agreement:false
};
const agreement = (state = initialState, action) => {
  switch (action.type) {
    case SET_AGREEMENT:
        return { agreement: !state.agreement };
    default:
      return state;
  }
};
export default agreement;