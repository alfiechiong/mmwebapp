import {  
  GET_DROPPED_FILES,
  SET_DROPPED_FILES,
  UPDATE_DROPPED_FILES
} from "../constants/action-types";
const initialState = {
  droppedFiles:[]
};
const droppedFiles = (state = initialState, action) => {
  switch (action.type) {
   
     case SET_DROPPED_FILES:
        return { ...state, droppedFiles: [...state.droppedFiles, action.payload] }; 
      case UPDATE_DROPPED_FILES:
        return { ...state, droppedFiles: [...state.droppedFiles, action.payload] }; 
    default:
      return state;
  }
};
export default droppedFiles;