import {  
  GET_DB_COURSES,
  GET_COURSES_FAILURE,
  GET_COURSES_SUCCESS,
  GET_COURSES_FETCHING,
  GET_DB_CUSTOM_COURSES,
  GET_CUSTOM_COURSES_FAILURE,
  GET_CUSTOM_COURSES_FETCHING,
  GET_CUSTOM_COURSES_SUCCESS 
} from "../constants/action-types";
const initialState = {
  course:[],
  dbCourses:[],
  dbCustomCourses:[],
  dbCoursesFetching:false,
  dbCoursesSuccess:false,
  dbCoursesFailure:false,
  dbCustomCoursesFetching:false,
  dbCustomCoursesSuccess:false,
  dbCustomCoursesFailure:false
};
const course = (state = initialState, action) => {
  switch (action.type) {
   
     case GET_DB_COURSES:
        return { ...state, dbCourses: action.payload}; 

      case GET_COURSES_FETCHING:
        return { ...state, dbCoursesFetching:  action.payload }; 

      case GET_COURSES_SUCCESS:
        return { ...state, dbCoursesSuccess: action.payload }; 

      case GET_COURSES_FAILURE:
        return { ...state, dbCoursesFailure:  action.payload }; 

      case GET_DB_CUSTOM_COURSES:
        return { ...state, dbCustomCourses: action.payload}; 

      case GET_CUSTOM_COURSES_FETCHING:
        return { ...state, dbCustomCoursesFetching:  action.payload }; 

      case GET_CUSTOM_COURSES_SUCCESS:
        return { ...state, dbCustomCoursesSuccess: action.payload }; 

      case GET_CUSTOM_COURSES_FAILURE:
        return { ...state, dbCustomCoursesFailure:  action.payload }; 
    default:
      return state;
  }
};
export default course;