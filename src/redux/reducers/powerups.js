import { ADD_DATA , SET_PAGE, SET_EDITING} from "../constants/action-types";
const initialState = {
  articles: [],
  data:[],
  page:0,
  editing:0
};
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_DATA:
        return { ...state, data:[action.payload] };
    case SET_PAGE:
        return { ...state, page:action.payload };
    case SET_EDITING:
        return {...state, editing:action.payload}
    default:
      return state;
  }
};
export default reducer;