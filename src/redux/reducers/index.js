import article from './article';
import session from './session';
import course from './course';
import user from './user';
import contact from './contact'
import {combineReducers} from 'redux'
import droppedFiles from './dropped'
import agreement from './aggreement'
import powerups from './powerups'
const Reducers = combineReducers(
  {
  article,
  session,
  course,
  user,
  contact,
  droppedFiles,
  agreement,
  powerups
  })

export default Reducers