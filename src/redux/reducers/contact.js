import {  
  GET_DB_CONTACT,
  GET_CONTACT_FAILURE,
  GET_CONTACT_SUCCESS,
  GET_CONTACT_FETCHING 
} from "../constants/action-types";
const initialState = {
  contact:[],
  dbContact:{},
  dbContactFetching:false,
  dbContactSuccess:false,
  dbContactFailure:false
};
const contact = (state = initialState, action) => {
  switch (action.type) {
   
     case GET_DB_CONTACT:
        return { ...state, dbContact: action.payload }; 

      case GET_CONTACT_FETCHING:
        return { ...state, dbContactFetching:  action.payload }; 

      case GET_CONTACT_SUCCESS:
        return { ...state, dbContactSuccess: action.payload }; 

      case GET_CONTACT_FAILURE:
        return { ...state, dbContactFailure:  action.payload }; 
    default:
      return state;
  }
};
export default contact;