import { createStore, compose, applyMiddleware } from "redux";
import Reducers from "../reducers";
import reduxThunk from 'redux-thunk';
import {reduxFirestore,getFirestore} from 'redux-firestore';
import {reactReduxFirebase,getFirebase} from 'react-redux-firebase';
import {devConfig} from '../../firebase/config';

const enhancer = compose(
    applyMiddleware(reduxThunk.withExtraArgument({getFirebase,getFirestore})),
   // reduxFirestore(devConfig),
   // reactReduxFirebase(devConfig),
    window.devToolsExtension ? window.devToolsExtension():f=>f
)(createStore)
//const store = createStore(Reducers,enhancer);
const store = enhancer(Reducers)
export default store;