import {  
    SET_DROPPED_FILES,
    GET_DROPPED_FILES,
    UPDATE_DROPPED_FILES

} from "../constants/action-types";
export const setDroppedFiles = files => (
    { type: SET_DROPPED_FILES, payload: files }
    );

    export const updateDroppedFiles = files => (
        { type: UPDATE_DROPPED_FILES, payload: files }
        );



