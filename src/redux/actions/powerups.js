import { ADD_DATA, SET_PAGE, SET_EDITING } from "../constants/action-types";
export const addData = data => ({ type: ADD_DATA, payload: data });
export const setPage = page => ({ type: SET_PAGE, payload: page });
export const setEditing = editing =>({type: SET_EDITING, payload:editing});
