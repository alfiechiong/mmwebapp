import { 
    GET_USERS,
    GET_USERS_FAILURE,
    GET_USERS_SUCCESS,
    GET_USERS_FETCHING,
    GET_DB_CURRENT_USER,
    GET_CURRENT_USER_FAILURE,
    GET_CURRENT_USER_FETCHING,
    GET_CURRENT_USER_SUCCESS,
    SET_CURRENT_USER 
} from "../constants/action-types";
import {GetAll, GetCurrentUser} from '../../components/childComponents/User/functions'

export const setCurrentUser = (data) =>{
    return {type:SET_CURRENT_USER, payload:data}
}

export const getCurrentUser = (email) =>{
    return (async (dispatch)=>{
        dispatch({type:GET_CURRENT_USER_FETCHING, payload:true})
        GetCurrentUser(email).then((res)=>{
           // console.log(res)
            dispatch({type:GET_CURRENT_USER_SUCCESS, payload:true})
            dispatch({type:GET_DB_CURRENT_USER, payload:res})
        }).catch(err=>{
            dispatch({type:GET_CURRENT_USER_FAILURE, payload:true})
        })
    })} 

export const getDbUsers = () =>{
    return (async (dispatch, getState, {getFirebase,getFirestore})=>{
        dispatch({type:GET_USERS_FETCHING, payload:true})
        GetAll().then((res)=>{
           // console.log(res)
            dispatch({type:GET_USERS_SUCCESS, payload:true})
            dispatch({type:GET_USERS, payload:res})
        }).catch(err=>{
            dispatch({type:GET_USERS_FAILURE, payload:true})
        })
    })} 
