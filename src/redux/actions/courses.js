import {  
    ADD_COURSES,
    GET_DB_COURSES,
    GET_COURSES_FAILURE,
    GET_COURSES_SUCCESS,
    GET_COURSES_FETCHING ,
    GET_DB_CUSTOM_COURSES,
    GET_CUSTOM_COURSES_FAILURE,
    GET_CUSTOM_COURSES_FETCHING,
    GET_CUSTOM_COURSES_SUCCESS
} from "../constants/action-types";
import {GetAll,GetMyCustomCourses} from '../../components/childComponents/Courses/functions'
export const addCourses = courses => ({ type: ADD_COURSES, payload: courses });


export const getDbCourses = () =>{
    return (async dispatch=>{
        dispatch({type:GET_COURSES_FETCHING, payload:true})
        GetAll().then((res)=>{
           // console.log(res)
            dispatch({type:GET_COURSES_SUCCESS, payload:true})
            dispatch({type:GET_DB_COURSES, payload:res})
        }).catch(err=>{
            dispatch({type:GET_COURSES_FAILURE, payload:true})
        })

    })} 

    export const getDbCustomCourses = (id) =>{
        return (async dispatch=>{
            dispatch({type:GET_CUSTOM_COURSES_FETCHING, payload:true})
           await GetMyCustomCourses(id).then((res)=>{
               // console.log(res)
                dispatch({type:GET_CUSTOM_COURSES_SUCCESS, payload:true})
                dispatch({type:GET_DB_CUSTOM_COURSES, payload:res})
            }).catch(err=>{
                dispatch({type:GET_CUSTOM_COURSES_FAILURE, payload:true})
            })
    
        })} 
