import {  
    ADD_CONTACT,
    GET_DB_CONTACT,
    GET_CONTACT_FAILURE,
    GET_CONTACT_SUCCESS,
    GET_CONTACT_FETCHING 
} from "../constants/action-types";
import {GetAll} from '../../components/childComponents/ContactInfo/functions'
export const addContact = contact => ({ type: ADD_CONTACT, payload: contact });


export const getDbContact = () =>{
    return (async dispatch=>{
        dispatch({type:GET_CONTACT_FETCHING, payload:true})
        GetAll().then((res)=>{
            dispatch({type:GET_CONTACT_SUCCESS, payload:true})
            dispatch({type:GET_DB_CONTACT, payload:res})
        }).catch(err=>{
            dispatch({type:GET_CONTACT_FAILURE, payload:true})
        })

    })} 
