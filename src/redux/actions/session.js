import { 
    ADD_ARTICLE, 
    ADD_UPLOADED_SESSION,
    GET_DB_SESSIONS,
    GET_SESSION_FAILURE,
    GET_SESSION_SUCCESS,
    GET_SESSION_FETCHING, 
    GET_DB_FREE_SESSIONS,
    GET_FREE_SESSION_FAILURE,
    GET_FREE_SESSION_SUCCESS,
    GET_FREE_SESSION_FETCHING, 
} from "../constants/action-types";
import {getAllSessions,getSessions,getFreeSessions} from '../../components/childComponents/Session/functions'
export const addArticle = article => ({ type: ADD_ARTICLE, payload: article });
export const addUploadedSession = session => ({ type: ADD_UPLOADED_SESSION, payload: session });


export const getDbSessions = () =>{
    return (async dispatch=>{
        dispatch({type:GET_SESSION_FETCHING, payload:true})
        //getSessions().then((res)=>{
        getAllSessions().then((res)=>{
           // console.log(res)
            dispatch({type:GET_SESSION_SUCCESS, payload:true})
            dispatch({type:GET_DB_SESSIONS, payload:res})
        }).catch(err=>{
            dispatch({type:GET_SESSION_FAILURE, payload:true})
        })

    })} 

    export const getDbFreeSessions = () =>{
        return (async dispatch=>{
            dispatch({type:GET_FREE_SESSION_FETCHING, payload:true})
            //getSessions().then((res)=>{
                getFreeSessions().then((res)=>{
               // console.log(res)
                dispatch({type:GET_FREE_SESSION_SUCCESS, payload:true})
                dispatch({type:GET_DB_FREE_SESSIONS, payload:res})
            }).catch(err=>{
                dispatch({type:GET_FREE_SESSION_FAILURE, payload:true})
            })
    
        })} 
