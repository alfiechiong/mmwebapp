import { 
    GET_DB_TALKPERSONS,
    GET_TALKPERSON_FAILURE,
    GET_TALKPERSON_SUCCESS,
    GET_TALKPERSON_FETCHING 
} from "../constants/action-types";
import {GetAll} from '../../components/childComponents/TalkPerson/functions'
export const getDbTalkPersons = () =>{
    return (async dispatch=>{
        dispatch({type:GET_TALKPERSON_FETCHING, payload:true})
        GetAll().then((res)=>{
           // console.log(res)
            dispatch({type:GET_TALKPERSON_SUCCESS, payload:true})
            dispatch({type:GET_DB_TALKPERSONS, payload:res})
        }).catch(err=>{
            dispatch({type:GET_TALKPERSON_FAILURE, payload:true})
        })

    })} 
