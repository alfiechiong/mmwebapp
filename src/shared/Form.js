import React, {
    Component,
    createRef
  } from 'react';
  import PropTypes from 'prop-types';
  import { auth } from '../firebase';
  import Button from '@material-ui/core/Button';
  import Typography from '@material-ui/core/Typography';



const styles = {
    card: {
      maxWidth: 345,
    },
    media: {
      height: 140,
    },
  };

  class Form extends Component {
    constructor(props) {
      super(props);
  
      this.email = createRef();
      this.password = createRef();
      this.handleSuccess = this.handleSuccess.bind(this);
      this.handleErrors = this.handleErrors.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }
  
    handleSuccess() {
      this.resetForm();
      this.props.onSuccess && this.props.onSuccess();
    }
  
    handleErrors(reason) {
      this.props.onError && this.props.onError(reason);
    }
  
    handleSubmit(event) {
      event.preventDefault();
      const {
        email,
        password,
        props: { action }
      } = this;
  
      auth.userSession(
        action,
        email.current.value,
        password.current.value
      ).then(this.handleSuccess).catch(this.handleErrors);
    }
  
    resetForm() {
      if (!this.email.current || !this.password.current) { return }
      const { email, password } = Form.defaultProps;
      this.email.current.value = email;
      this.password.current.value = password;
    }
  
    
    render() {
      return (
         
          <div style={styles.card}>
              <div>
        <form onSubmit={this.handleSubmit}>
        <Typography gutterBottom variant="h1" component="h2">
          {this.props.title}</Typography>
          <input
            name="name"
            type="email"
            ref={this.email}
          />  
          {/*  // <Input s={6} label="Email" type="email" ref={this.email}><Icon>account_circle</Icon></Input> */}

          <input
            name="password"
            type="password"
            autoComplete="none"
            ref={this.password}
          />

           
          <Button variant="contained" color="primary" type="submit">Submit</Button>
        </form>
        </div>
        </div>
      )
    }
  }
  
  Form.propTypes = {
    title: PropTypes.string.isRequired,
    action: PropTypes.string.isRequired,
    onSuccess: PropTypes.func,
    onError: PropTypes.func
  }
  
  Form.defaultProps = {
    errors: '',
    email: '',
    password: ''
  }
  
  export default Form;
  //export default Form;