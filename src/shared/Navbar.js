import React from 'react';
import {
  Link,
  withRouter
} from 'react-router-dom';
import { auth } from '../firebase';
import { Consumer } from '../components/AppProvider';
import Button from '@material-ui/core/Button';
//import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Dashboard from '../components/pages/dashboard';

const Navbar = props => {
  const handleLogout = context => {
    auth.logout();
    context.destroySession();
    props.history.push('/signedOut');
  };

  return <Consumer>
    {({ state, ...context }) => (
      state.currentUser ?
        <div>
        <Toolbar>
          <Button><Link to="/dashboard">Dashboard</Link></Button>
          <Button onClick={() => handleLogout(context)}>Logout</Button>
        </Toolbar>
        </div>
        :
        <Toolbar>
           <Button><Link to="/">Home</Link></Button>
          <Button><Link to="/login">Login</Link></Button>
          <Button><Link to="/signup">Create Account</Link></Button> 
        </Toolbar>
    )}
  </Consumer>
};

export default withRouter(Navbar);